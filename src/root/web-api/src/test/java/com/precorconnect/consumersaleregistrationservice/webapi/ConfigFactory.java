package com.precorconnect.consumersaleregistrationservice.webapi;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.identityservice.HmacKey;
import com.precorconnect.identityservice.HmacKeyImpl;

public class ConfigFactory {

    Config construct() {

        return new Config(
                constructIdOfExistingAccountWithSapAccountNumber(),
                constructIdentityServiceJwtSigningKey()
        );

    }

    private HmacKey constructIdentityServiceJwtSigningKey() {

        String identityServiceJwtSigningKey =
                System.getenv("TEST_IDENTITY_SERVICE_JWT_SIGNING_KEY");

        return
                new HmacKeyImpl(
                        identityServiceJwtSigningKey
                );

    }

    private AccountId constructIdOfExistingAccountWithSapAccountNumber() {

        String idOfExistingAccountWithSapAccountNumber =
                System.getenv("TEST_ID_OF_EXISTING_ACCOUNT_WITH_SAP_ACCOUNT_NUMBER");

        return
                new AccountIdImpl(
                        idOfExistingAccountWithSapAccountNumber
                );

    }

}
