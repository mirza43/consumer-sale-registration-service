package com.precorconnect.consumersaleregistrationservice.webapi;

import com.precorconnect.AccountId;
import com.precorconnect.identityservice.HmacKey;
import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

public class Config {

    /*
    fields
     */
    private final HmacKey identityServiceJwtSigningKey;

    private final AccountId idOfExistingAccountWithSapAccountNumber;

    /*
    constructors
     */
    public Config(
            final @NonNull AccountId idOfExistingAccountWithSapAccountNumber,
            final @NonNull HmacKey identityServiceJwtSigningKey
    ) {

        this.idOfExistingAccountWithSapAccountNumber =
                guardThat(
                        "idOfExistingAccountWithSapAccountNumber",
                        idOfExistingAccountWithSapAccountNumber
                )
                        .isNotNull()
                        .thenGetValue();

        this.identityServiceJwtSigningKey =
                guardThat(
                        "identityServiceJwtSigningKey",
                        identityServiceJwtSigningKey
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
    */
    public HmacKey getIdentityServiceJwtSigningKey() {
        return identityServiceJwtSigningKey;
    }

    public AccountId getIdOfExistingAccountWithSapAccountNumber() {
        return idOfExistingAccountWithSapAccountNumber;
    }
}
