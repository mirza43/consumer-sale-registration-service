package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerPostalAddress;

interface PostalAddressFactory {

    PostalAddress construct(
    		ConsumerPostalAddress postalAddress
    );

    ConsumerPostalAddress construct(
            @NonNull PostalAddress postalAddress
            );

}
