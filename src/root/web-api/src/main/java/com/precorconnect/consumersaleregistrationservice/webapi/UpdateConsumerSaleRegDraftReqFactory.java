package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateConsumerSaleRegDraftReqFactory {

	 com.precorconnect.consumersaleregistrationservice.UpdateConsumerSaleRegDraftReq construct(
	             com.precorconnect.consumersaleregistrationservice.webapi. @NonNull UpdateConsumerSaleRegDraftReq updatePartnerCommercialSaleRegDraftReq
	    );

}
