package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.consumersaleregistrationservice.PartnerSaleLineItemDto;
import com.precorconnect.consumersaleregistrationservice.PartnerSaleLineItemDtoImpl;
import com.precorconnect.consumersaleregistrationservice.SerialNumberImpl;





@Component
public class PartnerSaleSimpleLineItemFactoryImpl implements PartnerSaleSimpleLineItemFactory {

	@Override
	public PartnerSaleLineItemDto construct(
			@NonNull PartnerSaleSimpleLineItemWebDto partnerSaleLineItemWebDto
			) {
				
		return new PartnerSaleLineItemDtoImpl(new SerialNumberImpl(partnerSaleLineItemWebDto.getSerialNumber()));
	}

}
