package com.precorconnect.consumersaleregistrationservice.webapi;

import java.text.NumberFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class Application {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
        Runtime runtime = Runtime.getRuntime();
        final NumberFormat format = NumberFormat.getInstance();

        final long maxMemory = runtime.maxMemory();
        final long allocatedMemory = runtime.totalMemory();
        final long freeMemory = runtime.freeMemory();
        final long mb = 1024 * 1024;
        final String mega = "MB";
        LOGGER.info("========================== Memory Info ==========================");
        LOGGER.info("Free memory: " + format.format(freeMemory / mb) + mega);
        LOGGER.info("Allocated memory: " + format.format(allocatedMemory / mb) + mega);
        LOGGER.info("Max memory: " + format.format(maxMemory / mb) + mega);
        LOGGER.info("Total free memory: " + format.format((freeMemory + (maxMemory - allocatedMemory)) / mb) + mega);
        LOGGER.info("=================================================================\n");
        
       

    }
}
