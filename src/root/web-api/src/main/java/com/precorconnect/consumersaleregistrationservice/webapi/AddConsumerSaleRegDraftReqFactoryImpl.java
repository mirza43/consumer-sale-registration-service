package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.FirstName;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastName;
import com.precorconnect.LastNameImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPhoneNumber;
import com.precorconnect.consumersaleregistrationservice.ConsumerPhoneNumberImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPostalAddress;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItem;
import com.precorconnect.consumersaleregistrationservice.InvoiceNumber;
import com.precorconnect.consumersaleregistrationservice.InvoiceNumberImpl;
import com.precorconnect.consumersaleregistrationservice.InvoiceUrl;
import com.precorconnect.consumersaleregistrationservice.InvoiceUrlImpl;
import com.precorconnect.consumersaleregistrationservice.IsSubmit;
import com.precorconnect.consumersaleregistrationservice.IsSubmitImpl;
import com.precorconnect.consumersaleregistrationservice.PersonEmail;
import com.precorconnect.consumersaleregistrationservice.PersonEmailImpl;
import com.precorconnect.consumersaleregistrationservice.SellDate;
import com.precorconnect.consumersaleregistrationservice.SellDateImpl;


@Singleton
@Component
public class AddConsumerSaleRegDraftReqFactoryImpl
		implements AddConsumerSaleRegDraftReqFactory {
	
	/*
    fields
     */
    private final PostalAddressFactory postalAddressFactory;
    
    private final ConsumerSaleRegDraftSaleSimpleLineItemFactory consumerSaleRegDraftSaleSimpleLineItemFactory;
	
    private final ConsumerSaleRegDraftSaleCompositeLineItemFactory consumerSaleRegDraftSaleCompositeLineItemFactory;
	
    
    @Inject
    public AddConsumerSaleRegDraftReqFactoryImpl(
            @NonNull PostalAddressFactory postalAddressFactory,
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItemFactory consumerSaleRegDraftSaleSimpleLineItemFactory,
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItemFactory consumerSaleRegDraftSaleCompositeLineItemFactory
    ) {

        this.postalAddressFactory = 
				guardThat(
						"postalAddressFactory",
						postalAddressFactory
				)
						.isNotNull()
						.thenGetValue();
        
        this.consumerSaleRegDraftSaleSimpleLineItemFactory = 
				guardThat(
						"consumerSaleRegDraftSaleSimpleLineItemFactory",
						consumerSaleRegDraftSaleSimpleLineItemFactory
				)
						.isNotNull()
						.thenGetValue();
        
        this.consumerSaleRegDraftSaleCompositeLineItemFactory = 
				guardThat(
						"consumerSaleRegDraftSaleCompositeLineItemFactory",
						consumerSaleRegDraftSaleCompositeLineItemFactory
				)
						.isNotNull()
						.thenGetValue();

    }



	


	@Override
    public com.precorconnect.consumersaleregistrationservice.AddConsumerSaleRegDraftReq construct(
            @NonNull AddConsumerSaleRegDraftReq addConsumerCommercialSaleRegDraftReq
            
    ) {
		 try {

		
		 AccountId partnerAccountId =
				 new AccountIdImpl(
						 addConsumerCommercialSaleRegDraftReq
						 				.getPartnerAccountId()
						 );
		 
		 FirstName firstName =
	                new FirstNameImpl(
	                		addConsumerCommercialSaleRegDraftReq
	                		.getFirstName()
	                );
		 
		 LastName lastName =
	                new LastNameImpl(
	                		addConsumerCommercialSaleRegDraftReq
	                		.getLastName()
	                );

	        ConsumerPostalAddress address =
	                postalAddressFactory
	                        .construct(
	                        		addConsumerCommercialSaleRegDraftReq
	                        		.getAddress()
	                        		);

	        ConsumerPhoneNumber phoneNumber =
	                		addConsumerCommercialSaleRegDraftReq
	                		.getPhoneNumber()
	                		.map(ConsumerPhoneNumberImpl::new)
	                		.orElse(null);
	        
	        PersonEmail emailAddress=
	        				addConsumerCommercialSaleRegDraftReq
	        				.getPersonEmail()
	        				.map(PersonEmailImpl::new)
	    				 	.orElse(null);
	        			

	     InvoiceNumber invoiceNum =
	    		 new InvoiceNumberImpl(
	    				 addConsumerCommercialSaleRegDraftReq
	    				 	.getInvoiceNumber()
	    				 );

	     InvoiceUrl invoiceUrl =
	    				 addConsumerCommercialSaleRegDraftReq
	    				 	.getInvoiceUrl()
	    				 	.map(InvoiceUrlImpl::new)
	    				 	.orElse(null);

	     IsSubmit isSubmitted =
	    		 new IsSubmitImpl(
	    				 addConsumerCommercialSaleRegDraftReq
	    				 	.isSubmitted()
	    				 );



	      SellDate  sellDate =
					 new SellDateImpl(
							 new SimpleDateFormat("MM/dd/yyyy").parse(
									 addConsumerCommercialSaleRegDraftReq
									 .getSellDate()
							 	).toInstant()
							 );

	     UserId userId =
	    				 addConsumerCommercialSaleRegDraftReq
	    				 	.getPartnerRepUserId()
	    				 	.map(UserIdImpl::new)
	    				 	.orElse(null);


	     Collection<ConsumerSaleLineItem> saleLineItems = new ArrayList<ConsumerSaleLineItem>();

	     if(addConsumerCommercialSaleRegDraftReq.getSimpleLineItems().isPresent()){
		     saleLineItems.addAll(
			     StreamSupport
		         .stream(
		        		 addConsumerCommercialSaleRegDraftReq.getSimpleLineItems().get()
		                         .spliterator(),
		                 false)
		         .map(consumerSaleRegDraftSaleSimpleLineItemFactory::construct)
		         .collect(Collectors.toList())
		     );
	     }
	     if(addConsumerCommercialSaleRegDraftReq.getCompositeLineItems().isPresent()){
		     saleLineItems.addAll(
			     StreamSupport
		         .stream(
		        		 addConsumerCommercialSaleRegDraftReq.getCompositeLineItems().get()
		                         .spliterator(),
		                 false)
		         .map(consumerSaleRegDraftSaleCompositeLineItemFactory::construct)
		         .collect(Collectors.toList())
	    	);
	     }
	     
	     return 
	    		new com.precorconnect.consumersaleregistrationservice.AddConsumerSaleRegDraftReqImpl(
	    				 firstName,
	    				 lastName,
	    				 address,
	    				 phoneNumber,
	    				 emailAddress,
	    				 partnerAccountId,
	    				 invoiceNum,
	    				 invoiceUrl,
	    				 isSubmitted,
	    				 sellDate,
	    				 userId,
	    				 saleLineItems
	    				 );

		 } catch (ParseException e) {
				e.printStackTrace();
				return null;
			}

    }

}
