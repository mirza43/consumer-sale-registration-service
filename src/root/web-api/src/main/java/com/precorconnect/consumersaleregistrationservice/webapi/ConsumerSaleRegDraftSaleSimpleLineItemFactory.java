package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface ConsumerSaleRegDraftSaleSimpleLineItemFactory {


	com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem construct(
	            @NonNull ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem
	    );

	 ConsumerSaleRegDraftSaleSimpleLineItem construct(
			 com.precorconnect.consumersaleregistrationservice.
			 @NonNull ConsumerSaleRegDraftSaleSimpleLineItem consumerSaleRegDraftSaleSimpleLineItem
	    );

	 com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem construct(
	            @NonNull UpdateConsumerSaleRegDraftSaleSimpleLineItem updateConsumerSaleRegDraftSaleSimpleLineItemWebReq
	    );


	 UpdateConsumerSaleRegDraftSaleSimpleLineItem constructUpdateSaleRegDraftObj(
			 com.precorconnect.consumersaleregistrationservice.
			 @NonNull ConsumerSaleRegDraftSaleSimpleLineItem partnerSaleRegDraftSaleSimpleLineItem
	    );

}
