package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface AddConsumerSaleRegDraftReqFactory {


	 com.precorconnect.consumersaleregistrationservice.AddConsumerSaleRegDraftReq construct(
	            @NonNull AddConsumerSaleRegDraftReq addPartnerRepReq
	    );
}
