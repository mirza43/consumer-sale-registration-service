package com.precorconnect.consumersaleregistrationservice.webapi;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Component;

import com.precorconnect.consumersaleregistrationservice.PartnerCompositeSaleLineItemDto;
import com.precorconnect.consumersaleregistrationservice.PartnerCompositeSaleLineItemDtoImpl;
import com.precorconnect.consumersaleregistrationservice.PartnerSaleLineItemDto;
import com.precorconnect.consumersaleregistrationservice.PartnerSaleLineItemDtoImpl;
import com.precorconnect.consumersaleregistrationservice.SerialNumberImpl;





@Component
public class PartnerSaleCompositeLineItemFactoryImpl 
			implements PartnerSaleCompositeLineItemFactory {

	@Override
	public PartnerCompositeSaleLineItemDto construct(
			PartnerSaleCompositeLineItemWebDto partnerSaleLineSimpleItemWebDto
			) {
		
		Collection<PartnerSaleLineItemDto> components = new ArrayList<PartnerSaleLineItemDto>();
		for(PartnerSaleSimpleLineItemWebDto lineItemWebDto : partnerSaleLineSimpleItemWebDto.getComponents()) {
		PartnerSaleLineItemDto saleLineItemDto = new PartnerSaleLineItemDtoImpl(new SerialNumberImpl(lineItemWebDto.getSerialNumber()));
		components.add(saleLineItemDto);
		}
		return new PartnerCompositeSaleLineItemDtoImpl(components);
	}

}
