package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.stereotype.Component;

import com.precorconnect.FirstName;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.LastName;
import com.precorconnect.LastNameImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;
import com.precorconnect.consumersaleregistrationservice.PartnerRep;
import com.precorconnect.consumersaleregistrationservice.PartnerRepImpl;
import com.precorconnect.consumersaleregistrationservice.PersonEmail;
import com.precorconnect.consumersaleregistrationservice.PersonEmailImpl;
import com.precorconnect.consumersaleregistrationservice.SubmitConsumerSaleRegDraftRequestDto;
import com.precorconnect.consumersaleregistrationservice.SubmitConsumerSaleRegDraftRequestDtoImpl;
import com.precorconnect.consumersaleregistrationservice.SubmittedByName;
import com.precorconnect.consumersaleregistrationservice.SubmittedByNameImpl;

@Component
public class SubmitConsumerSaleRegDraftRequestFactoryImpl 
		implements SubmitConsumerSaleRegDraftRequestFactory {

	
	@Override
	public SubmitConsumerSaleRegDraftRequestDto construct(
			@NonNull SubmitConsumerSaleRegDraftDto submitConsumerSaleRegDraftDto
			) {
		
		ConsumerSaleRegDraftId consumerSaleRegDraftId = 
				new ConsumerSaleRegDraftIdImpl(
						submitConsumerSaleRegDraftDto
							.getId()
						);
		
		SubmittedByName submittedByName = 
				new SubmittedByNameImpl(
						submitConsumerSaleRegDraftDto
							.getSubmittedByName()
						);
		
		PartnerRep partnerRepId = 
				new PartnerRepImpl(
						submitConsumerSaleRegDraftDto
							.getPartnerRepId().isPresent()?
						submitConsumerSaleRegDraftDto
							.getPartnerRepId()
							.get():null
						);
		
		FirstName firstName = 
				new FirstNameImpl(
						submitConsumerSaleRegDraftDto
							.getFirstName().isPresent()?
						submitConsumerSaleRegDraftDto
							.getFirstName()
							.get():null
						);
		
		LastName lastName = 
				new LastNameImpl(
						submitConsumerSaleRegDraftDto
							.getLastName().isPresent()?
							submitConsumerSaleRegDraftDto
							.getLastName()
							.get():null
						);
		
		PersonEmail emailAddress = 
				new PersonEmailImpl(
						submitConsumerSaleRegDraftDto
							.getEmailAddress().isPresent()?
							submitConsumerSaleRegDraftDto
							.getEmailAddress()
							.get():null
						);
		
		return 
				new SubmitConsumerSaleRegDraftRequestDtoImpl(
						consumerSaleRegDraftId,
						submittedByName,
						partnerRepId,
						firstName,
						lastName,
						emailAddress
						);
		
	}

}
