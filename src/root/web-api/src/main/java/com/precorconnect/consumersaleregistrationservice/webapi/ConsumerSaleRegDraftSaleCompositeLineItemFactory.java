package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;


public interface ConsumerSaleRegDraftSaleCompositeLineItemFactory {


	com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem construct(
	            @NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
	    );

	ConsumerSaleRegDraftSaleCompositeLineItem construct(
			com.precorconnect.consumersaleregistrationservice.
			@NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
    );

	com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem construct(
            @NonNull UpdateConsumerSaleRegDraftSaleCompositeLineItem updatePartnerSaleRegDraftSaleCompositeLineItemWebReq
    );

	UpdateConsumerSaleRegDraftSaleCompositeLineItem constructUpdateSaleRegDraftObj(
			com.precorconnect.consumersaleregistrationservice.
			@NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
    );

}
