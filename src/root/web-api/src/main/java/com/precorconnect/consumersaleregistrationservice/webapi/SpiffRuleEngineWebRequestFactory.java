package com.precorconnect.consumersaleregistrationservice.webapi;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ProductSaleRegistrationRuleEngineRequestDto;



public interface SpiffRuleEngineWebRequestFactory {
	
	ProductSaleRegistrationRuleEngineRequestDto construct(
			@NonNull ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto
			);

}
