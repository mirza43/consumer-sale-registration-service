package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.PartnerSaleInvoiceId;

import java.util.Optional;

public interface UpdateSaleInvoiceIdOfConsumerSaleRegDraftReq {

    ConsumerSaleRegDraftId getPartnerSaleRegDraftId();

    Optional<PartnerSaleInvoiceId> getSaleInvoiceId();

}
