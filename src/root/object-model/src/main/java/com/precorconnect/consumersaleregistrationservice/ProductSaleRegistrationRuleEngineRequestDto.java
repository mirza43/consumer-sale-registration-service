package com.precorconnect.consumersaleregistrationservice;

import java.util.List;
import java.util.Optional;

public interface ProductSaleRegistrationRuleEngineRequestDto {
	
	Optional<PartnerCommericalSaleDraftId> getPartnerSaleRegId();

	SellDate getSellDate();

	Optional<InstallDate> getInstallDate();
	
	Optional<SubmittedDate> getSubmittedDate();
	
	Optional<Brand> getBrand();
	
	List<PartnerSaleLineItemDto> getSimpleLineItems();
	
	List<PartnerCompositeSaleLineItemDto> getCompositeLineItems();
	
	Optional<Amount> getSpiffAmount();
}
