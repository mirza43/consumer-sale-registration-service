package com.precorconnect.consumersaleregistrationservice;

public interface SubmittedDate {
	
	String getValue();

}
