package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public abstract class AbstractConsumerSaleRegDraftSynopsisView
        implements ConsumerSaleRegDraftSynopsisView {

    /*
    fields
     */
    private final ConsumerSaleRegDraftId id;

    private final AccountId partnerAccountId;


    /*
    constructors
     */
    public AbstractConsumerSaleRegDraftSynopsisView(
            @NonNull ConsumerSaleRegDraftId id,
            @NonNull AccountId partnerAccountId
    ) {

    	 this.id =
                 guardThat(
                         "id",
                          id
                 )
                         .isNotNull()
                         .thenGetValue();

    	 this.partnerAccountId =
                 guardThat(
                         "partnerAccountId",
                          partnerAccountId
                 )
                         .isNotNull()
                         .thenGetValue();
    	

    }

    /*
    getter methods
     */
    @Override
    public ConsumerSaleRegDraftId getId() {
        return id;
    }

    @Override
    public AccountId getPartnerAccountId() {
        return partnerAccountId;
    }
    /*
    equality methods
     */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((partnerAccountId == null) ? 0 : partnerAccountId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractConsumerSaleRegDraftSynopsisView other = (AbstractConsumerSaleRegDraftSynopsisView) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (partnerAccountId == null) {
			if (other.partnerAccountId != null)
				return false;
		} else if (!partnerAccountId.equals(other.partnerAccountId))
			return false;
		return true;
	}

    
}
