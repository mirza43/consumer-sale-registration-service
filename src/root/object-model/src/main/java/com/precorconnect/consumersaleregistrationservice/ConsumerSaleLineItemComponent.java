package com.precorconnect.consumersaleregistrationservice;

import java.math.BigDecimal;
import java.util.Optional;

import com.precorconnect.AssetId;
import com.precorconnect.AssetSerialNumber;
import com.precorconnect.ProductGroupId;
import com.precorconnect.ProductGroupName;
import com.precorconnect.ProductLineId;
import com.precorconnect.ProductLineName;

	
public interface ConsumerSaleLineItemComponent extends ConsumerSaleLineItem {
	
	ConsumerSaleRegDraftSaleLineItemId getId();

	AssetId getAssetId();

	AssetSerialNumber getSerialNumber();

	ProductLineId getProductLineId();

	Optional<BigDecimal> getPrice();

	ProductLineName getProductLineName();

	Optional<ProductGroupId> getProductGroupId();

	Optional<ProductGroupName> getProductGroupName();
	
	ProductName getProductName();

}
