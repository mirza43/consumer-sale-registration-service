package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

public class ConsumerSaleRegDraftSaleLineItemIdImpl
        implements ConsumerSaleRegDraftSaleLineItemId {

    /*
    fields
     */
    private final Long value;

    /*
    constructors
     */
    public ConsumerSaleRegDraftSaleLineItemIdImpl(
            @NonNull Long value
    ) throws IllegalArgumentException {

    	this.value = value;

    }

    /*
    getter methods
     */
    @Override
    public Long getValue() {
        return value;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        ConsumerSaleRegDraftSaleLineItemIdImpl that = (ConsumerSaleRegDraftSaleLineItemIdImpl) o;

        return value.equals(that.value);

    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
