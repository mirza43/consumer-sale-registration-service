package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.time.Instant;
import java.util.Collection;
import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.AccountId;
import com.precorconnect.FirstName;
import com.precorconnect.LastName;
import com.precorconnect.UserId;

public abstract class AbstractConsumerSaleRegDraftView
        implements ConsumerSaleRegDraftView {

    /*
    fields
     */
    private final ConsumerSaleRegDraftId id;
    
    private final  FirstName firstName;
    
    private final LastName lastName;

    private final ConsumerPostalAddress address;

    private final ConsumerPhoneNumber phoneNumber;
   
    private final PersonEmail emailAddress;

    private final AccountId partnerAccountId;

    private final Collection<ConsumerSaleRegDraftSaleLineItem> saleLineItems;

    private final InvoiceNumber invoiceNumber;

    private final Instant saleCreatedAtTimestamp;

    private final Instant sellDate;

    private final UserId salePartnerRepId;

    private final InvoiceUrl invoiceUrl;

    private final IsSubmit isSubmitted;
    
    private final Instant createDate;

    /*
        constructors
         */
    public AbstractConsumerSaleRegDraftView(
            @NonNull ConsumerSaleRegDraftId id,
            @NonNull FirstName firstName,
	        @NonNull LastName lastName,
	        @NonNull ConsumerPostalAddress address,
	        @NonNull ConsumerPhoneNumber phoneNumber,
	        @Nullable PersonEmail emailAddress,
            @NonNull AccountId partnerAccountId,
            @NonNull Collection<ConsumerSaleRegDraftSaleLineItem> saleLineItems,
            @Nullable InvoiceNumber invoiceNumber,
            @NonNull Instant saleCreatedAtTimestamp,
            @NonNull Instant sellDate,
            @Nullable UserId salePartnerRepId,
            @Nullable InvoiceUrl invoiceUrl,
            @NonNull IsSubmit isSubmitted,
            @NonNull Instant createDate
    ) {

    	this.id =
                guardThat(
                        "id",
                         id
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.firstName = 
 				guardThat(
 						"firstName",
 						firstName
 				)
 						.isNotNull()
 						.thenGetValue();
    	 
    	 this.lastName = 
  				guardThat(
  						"lastName",
  						lastName
  				)
  						.isNotNull()
  						.thenGetValue();

         this.address = 
 				guardThat(
 						"address",
 						address
 				)
 						.isNotNull()
 						.thenGetValue();

         this.phoneNumber = phoneNumber; 
 				
         
         this.emailAddress = emailAddress;
  				

    	this.partnerAccountId =
                guardThat(
                        "partnerAccountId",
                         partnerAccountId
                )
                        .isNotNull()
                        .thenGetValue();

    	
    	this.sellDate =
                guardThat(
                        "sellDate",
                        sellDate
                )
                        .isNotNull()
                        .thenGetValue();

    	

    	this.saleCreatedAtTimestamp =
                guardThat(
                        "saleCreatedAtTimestamp",
                        saleCreatedAtTimestamp
                )
                        .isNotNull()
                        .thenGetValue();

    	this.saleLineItems =
                (Collection<ConsumerSaleRegDraftSaleLineItem>) guardThat(
                        "saleLineItems",
                        saleLineItems
                )
                        .isNotNull()
                        .thenGetValue();

    	this.invoiceNumber =
                guardThat(
                        "invoiceNumber",
                        invoiceNumber
                )
                        .isNotNull()
                        .thenGetValue();

    	this.isSubmitted =
                guardThat(
                        "isSubmitted",
                        isSubmitted
                )
                        .isNotNull()
                        .thenGetValue();


        this.salePartnerRepId = salePartnerRepId;

        this.invoiceUrl = invoiceUrl;
        
        this.createDate=createDate;
    }

    /*
    getter methods
     */
    @Override
    public ConsumerSaleRegDraftId getId() {
        return id;
    }
    
    public FirstName getFirstName() {
		return firstName;
	}

	public LastName getLastName() {
		return lastName;
	}

	public ConsumerPostalAddress getAddress() {
		return address;
	}

	public Optional<ConsumerPhoneNumber> getPhoneNumber() {
		return Optional.ofNullable(phoneNumber);
	}

	public Optional<PersonEmail> getPersonEmail() {
		return Optional.ofNullable(emailAddress);
	}

	public IsSubmit getIsSubmitted() {
		return isSubmitted;
	}

    @Override
    public AccountId getPartnerAccountId() {
        return partnerAccountId;
    }

    @Override
    public Collection<ConsumerSaleRegDraftSaleLineItem> getSaleLineItems() {
        return saleLineItems;
    }

    @Override
    public InvoiceNumber getInvoiceNumber() {
        return invoiceNumber;
    }

    @Override
    public Instant getSaleCreatedAtTimestamp() {
        return saleCreatedAtTimestamp;
    }

    @Override
    public Instant getSellDate() {
        return sellDate;
    }

    @Override
	public Optional<UserId> getSalePartnerRepId() {
        return Optional.ofNullable(salePartnerRepId);
    }


	@Override
	public IsSubmit isSubmitted() {
		return isSubmitted;
	}

	@Override
	public Optional<InvoiceUrl> getInvoiceUrl() {
		return Optional.ofNullable(invoiceUrl);
	}
	
	@Override
	public Instant getCreateDate() {
		return createDate;
	}
	 /*
    equality methods
    */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result + ((invoiceUrl == null) ? 0 : invoiceUrl.hashCode());
		result = prime * result + ((isSubmitted == null) ? 0 : isSubmitted.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((partnerAccountId == null) ? 0 : partnerAccountId.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((saleCreatedAtTimestamp == null) ? 0 : saleCreatedAtTimestamp.hashCode());
		result = prime * result + ((saleLineItems == null) ? 0 : saleLineItems.hashCode());
		result = prime * result + ((salePartnerRepId == null) ? 0 : salePartnerRepId.hashCode());
		result = prime * result + ((sellDate == null) ? 0 : sellDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractConsumerSaleRegDraftView other = (AbstractConsumerSaleRegDraftView) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} 
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} 
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null)
				return false;
		} else if (!invoiceNumber.equals(other.invoiceNumber))
			return false;
		if (invoiceUrl == null) {
			if (other.invoiceUrl != null)
				return false;
		} else if (!invoiceUrl.equals(other.invoiceUrl))
			return false;
		if (isSubmitted == null) {
			if (other.isSubmitted != null)
				return false;
		} else if (!isSubmitted.equals(other.isSubmitted))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (partnerAccountId == null) {
			if (other.partnerAccountId != null)
				return false;
		} else if (!partnerAccountId.equals(other.partnerAccountId))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (saleCreatedAtTimestamp == null) {
			if (other.saleCreatedAtTimestamp != null)
				return false;
		} 
		if (saleLineItems == null) {
			if (other.saleLineItems != null)
				return false;
		} else if (!saleLineItems.equals(other.saleLineItems))
			return false;
		if (salePartnerRepId == null) {
			if (other.salePartnerRepId != null)
				return false;
		} else if (!salePartnerRepId.equals(other.salePartnerRepId))
			return false;
		if (sellDate == null) {
			if (other.sellDate != null)
				return false;
		} 
		return true;
	}
}
