package com.precorconnect.consumersaleregistrationservice;

import java.util.Optional;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;

public interface SubmitConsumerSaleRegDraftRequestDto {

	ConsumerSaleRegDraftId getId();
	
	SubmittedByName getSubmittedByName();

	Optional<PartnerRep> getPartnerRep();
	
	Optional<FirstName> getFirstName();
	
	Optional<LastName> getLastName();
	
	Optional<PersonEmail> getEmailAddress();
	
}
