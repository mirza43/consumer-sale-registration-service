package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AccountName;
import com.precorconnect.PostalAddress;
import com.precorconnect.SapAccountNumber;

public class GenerallAccountViewImpl implements GeneralAccountView {
	
	/*
    fields
     */
    private final AccountId id;

    private final AccountName name;

    private final PostalAddress address;

    private final SapAccountNumber sapAccountNumber;
    
    /*
    constructors
     */
    public GenerallAccountViewImpl(
            @NonNull AccountId id,
            @NonNull AccountName name,
            @NonNull PostalAddress address,
            @NonNull SapAccountNumber sapAccountNumber
    ) {

        this.id = 
				guardThat(
						"id",
						id
				)
						.isNotNull()
						.thenGetValue();

        this.name = 
				guardThat(
						"name",
						name
				)
						.isNotNull()
						.thenGetValue();

        this.address = 
				guardThat(
						"address",
						address
				)
						.isNotNull()
						.thenGetValue();

        this.sapAccountNumber = sapAccountNumber;
        
    }

    /*
    getter methods
     */
    @Override
    public AccountId getId() {
        return id;
    }

    @Override
    public AccountName getName() {
        return name;
    }

    @Override
    public PostalAddress getAddress() {
        return address;
    }

    @Override
    public Optional<SapAccountNumber> getSapAccountNumber() {
        return Optional.ofNullable(sapAccountNumber);
    }
    
    
  

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenerallAccountViewImpl other = (GenerallAccountViewImpl) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sapAccountNumber == null) {
			if (other.sapAccountNumber != null)
				return false;
		} else if (!sapAccountNumber.equals(other.sapAccountNumber))
			return false;
		return true;
	}

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime
				* result
				+ ((sapAccountNumber == null) ? 0 : sapAccountNumber.hashCode());
		return result;
	}

}
