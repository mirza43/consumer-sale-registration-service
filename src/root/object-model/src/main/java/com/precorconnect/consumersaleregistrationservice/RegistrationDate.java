package com.precorconnect.consumersaleregistrationservice;

import java.time.Instant;

public interface RegistrationDate {

	Instant getValue();
}
