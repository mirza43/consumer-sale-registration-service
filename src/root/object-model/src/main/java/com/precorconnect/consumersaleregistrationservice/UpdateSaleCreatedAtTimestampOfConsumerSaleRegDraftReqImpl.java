package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.time.Instant;
import java.util.Optional;

public class UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReqImpl
        implements UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReq {

    /*
    fields
     */
    private final ConsumerSaleRegDraftId partnerSaleRegDraftId;

    private final Instant saleCreatedAtTimestamp;

    /*
    constructors
     */
    public UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReqImpl(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId,
            @NonNull Instant saleCreatedAtTimestamp
    ) {

    	this.partnerSaleRegDraftId =
                guardThat(
                        "partnerSaleRegDraftId",
                         partnerSaleRegDraftId
                )
                        .isNotNull()
                        .thenGetValue();
       
        this.saleCreatedAtTimestamp = saleCreatedAtTimestamp;

    }

    /*
    getter methods
     */
    @Override
    public ConsumerSaleRegDraftId getConsumerSaleRegDraftId() {
        return partnerSaleRegDraftId;
    }

    @Override
    public Optional<Instant> getSaleCreatedAtTimestamp() {
        return Optional.ofNullable(saleCreatedAtTimestamp);
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReqImpl that = (UpdateSaleCreatedAtTimestampOfConsumerSaleRegDraftReqImpl) o;

        if (!partnerSaleRegDraftId.equals(that.partnerSaleRegDraftId)) return false;
        return !(saleCreatedAtTimestamp != null ? !saleCreatedAtTimestamp.equals(that.saleCreatedAtTimestamp) : that.saleCreatedAtTimestamp != null);

    }

    @Override
    public int hashCode() {
        int result = partnerSaleRegDraftId.hashCode();
        result = 31 * result + (saleCreatedAtTimestamp != null ? saleCreatedAtTimestamp.hashCode() : 0);
        return result;
    }
}
