package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReqImpl
        implements UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq {

    /*
    fields
     */
    private final ConsumerSaleRegDraftId consumerSaleRegDraftId;

    private final InvoiceUrl invoiceUrl;

    /*
    constructors
     */
    public UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReqImpl(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
            @NonNull InvoiceUrl invoiceUrl
    ) {

    	this.consumerSaleRegDraftId =
                guardThat(
                        "consumerSaleRegDraftId",
                        consumerSaleRegDraftId
                )
                        .isNotNull()
                        .thenGetValue();

        this.invoiceUrl = invoiceUrl;

    }

    /*
    getter methods
     */
    @Override
    public InvoiceUrl getInvoiceUrl() {
        return invoiceUrl;
    }

    @Override
    public ConsumerSaleRegDraftId getConsumerSaleRegDraftId() {
        return consumerSaleRegDraftId;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReqImpl that = (UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReqImpl) o;

        if (!consumerSaleRegDraftId.equals(that.consumerSaleRegDraftId)) {
			return false;
		}
        return !(invoiceUrl != null ? !invoiceUrl.equals(that.invoiceUrl) : that.invoiceUrl != null);

    }

    @Override
    public int hashCode() {
        int result = consumerSaleRegDraftId.hashCode();
        result = 31 * result + (invoiceUrl != null ? invoiceUrl.hashCode() : 0);
        return result;
    }
}
