package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class UpdateSaleAccountIdOfConsumerSaleRegDraftReqImpl
        implements UpdateSaleAccountIdOfConsumerSaleRegDraftReq {

    /*
    fields
     */
    private final AccountId saleAccountId;

    private final ConsumerSaleRegDraftId consumerSaleRegDraftId;

    /*
    constructors
     */
    public UpdateSaleAccountIdOfConsumerSaleRegDraftReqImpl(
            @NonNull AccountId saleAccountId,
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId
    ) {

    	this.saleAccountId =
                 guardThat(
                         "saleAccountId",
                          saleAccountId
                 )
                         .isNotNull()
                         .thenGetValue();

        this.consumerSaleRegDraftId =
                guardThat(
                        "consumerSaleRegDraftId",
                        consumerSaleRegDraftId
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    public AccountId getSaleAccountId() {
        return saleAccountId;
    }

    @Override
    public ConsumerSaleRegDraftId getConsumerSaleRegDraftId() {
        return consumerSaleRegDraftId;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UpdateSaleAccountIdOfConsumerSaleRegDraftReqImpl that = (UpdateSaleAccountIdOfConsumerSaleRegDraftReqImpl) o;

        if (!saleAccountId.equals(that.saleAccountId)) return false;
        return consumerSaleRegDraftId.equals(that.consumerSaleRegDraftId);

    }

    @Override
    public int hashCode() {
        int result = saleAccountId.hashCode();
        result = 31 * result + consumerSaleRegDraftId.hashCode();
        return result;
    }
}
