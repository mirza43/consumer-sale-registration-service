package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class ConsumerSaleRegDraftIdImpl
        implements ConsumerSaleRegDraftId {

    /*
    fields
     */
    private final Long value;

    /*
    constructors
    */
    public ConsumerSaleRegDraftIdImpl(
            @NonNull Long value
    ) {

    	this.value =
                guardThat(
                        "value",
                         value
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public Long getValue() {
        return value;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConsumerSaleRegDraftIdImpl that = (ConsumerSaleRegDraftIdImpl) o;

        return value.equals(that.value);

    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
