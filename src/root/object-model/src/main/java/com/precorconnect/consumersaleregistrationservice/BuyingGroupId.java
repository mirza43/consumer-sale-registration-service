package com.precorconnect.consumersaleregistrationservice;

/**
 * represents a unique identifier for a management company
 */
public interface BuyingGroupId {

    String getValue();

}
