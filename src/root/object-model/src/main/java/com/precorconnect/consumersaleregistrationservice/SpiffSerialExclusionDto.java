package com.precorconnect.consumersaleregistrationservice;

import java.time.Instant;

public interface SpiffSerialExclusionDto {
	
	SpiffSerialExclusionId getSpiffSerialExclusionId();
	
	SerialNumber getSerialNumber();
	
	Description getDescription();
	
	Instant getDateCreate();

}
