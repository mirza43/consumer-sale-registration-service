package com.precorconnect.consumersaleregistrationservice;


public class ConsumerPhoneNumberImpl implements
        ConsumerPhoneNumber {

    /*
    fields
     */
    private final String value;

    /*
    constructors
     */
    public ConsumerPhoneNumberImpl(
            String value
    )  {

        this.value = value;
               

    }

    /*
    getter methods
     */
    @Override
    public String getValue() {
        return value;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConsumerPhoneNumberImpl that = (ConsumerPhoneNumberImpl) o;

        if (!value.equals(that.value)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }
}
