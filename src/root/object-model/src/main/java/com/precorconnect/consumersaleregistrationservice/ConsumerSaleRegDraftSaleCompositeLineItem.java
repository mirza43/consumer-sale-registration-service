package com.precorconnect.consumersaleregistrationservice;


/**
 * a decorator that adds a unique identifier to
 * {@link PartnerSaleCompositeLineItem}
 * to allow external reference
 */
public interface ConsumerSaleRegDraftSaleCompositeLineItem
        extends ConsumerSaleCompositeLineItem, ConsumerSaleRegDraftSaleLineItem {

}
