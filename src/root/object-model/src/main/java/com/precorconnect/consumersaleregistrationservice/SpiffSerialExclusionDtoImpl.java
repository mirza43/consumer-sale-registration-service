package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.time.Instant;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

public class SpiffSerialExclusionDtoImpl implements SpiffSerialExclusionDto {
	
	/**
	 * fields
	 */
	
	private final SpiffSerialExclusionId spiffSerialExclusionId;
	
	private final SerialNumber serialNumber;
	
	private final Description description;
	
	private final Instant dateCreate;
	
	/**
	 * Constructors
	 * @param spiffSerialExclusionId
	 * @param serialNumber
	 * @param description
	 * @param dateCreate
	 */
	public SpiffSerialExclusionDtoImpl(
			@NonNull SpiffSerialExclusionId spiffSerialExclusionId,
			@NonNull SerialNumber serialNumber, 
			@Nullable Description description, 
			@NonNull Instant dateCreate) {
		
		this.spiffSerialExclusionId =
                guardThat(
                        "spiffSerialExclusionId",
                        spiffSerialExclusionId
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.serialNumber = 
				 guardThat(
	                        "serialNumber",
	                        serialNumber
	                )
	                        .isNotNull()
	                        .thenGetValue();
		
		this.description = description;
		
		this.dateCreate = 
				 guardThat(
	                        "dateCreate",
	                        dateCreate
	                )
	                        .isNotNull()
	                        .thenGetValue();
	}
	
	
	/**
	 * getter methods
	 */

	@Override
	public SpiffSerialExclusionId getSpiffSerialExclusionId() {
		return spiffSerialExclusionId;
	}

	@Override
	public SerialNumber getSerialNumber() {
		return serialNumber;
	}

	@Override
	public Description getDescription() {
		return description;
	}

	@Override
	public Instant getDateCreate() {
		return dateCreate;
	}

	/**
	 * equality methods
	 */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateCreate == null) ? 0 : dateCreate.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
		result = prime * result + ((spiffSerialExclusionId == null) ? 0 : spiffSerialExclusionId.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpiffSerialExclusionDtoImpl other = (SpiffSerialExclusionDtoImpl) obj;
		if (dateCreate == null) {
			if (other.dateCreate != null)
				return false;
		} else if (!dateCreate.equals(other.dateCreate))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (serialNumber == null) {
			if (other.serialNumber != null)
				return false;
		} else if (!serialNumber.equals(other.serialNumber))
			return false;
		if (spiffSerialExclusionId == null) {
			if (other.spiffSerialExclusionId != null)
				return false;
		} else if (!spiffSerialExclusionId.equals(other.spiffSerialExclusionId))
			return false;
		return true;
	}
	
	

}
