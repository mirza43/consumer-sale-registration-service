package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.CityName;
import com.precorconnect.Iso31661Alpha2Code;
import com.precorconnect.Iso31662Code;
import com.precorconnect.PostalCode;
import com.precorconnect.StreetAddress;

public class ConsumerPostalAddressImpl
        implements ConsumerPostalAddress {

    /*
    fields
     */
    private final StreetAddress addressLine1;
    
    private final AddressLine addressLine2;

    private final CityName city;

    private final Iso31662Code regionIso31662Code;

    private final PostalCode postalCode;

    private final Iso31661Alpha2Code countryIso31661Alpha2Code;

    /*
    constructors
     */
    public ConsumerPostalAddressImpl(
            @NonNull StreetAddress addressLine1,
            @Nullable AddressLine addressLine2,
            @NonNull CityName city,
            @NonNull Iso31662Code regionIso31662Code,
            @NonNull PostalCode postalCode,
            @NonNull Iso31661Alpha2Code countryIso31661Alpha2Code
    ) {

        this.addressLine1 =
                guardThat(
                        "addressLine1",
                        addressLine1
                )
                        .isNotNull()
                        .thenGetValue();
        
        this.addressLine2 = addressLine2;

        this.city =
                guardThat(
                        "city",
                        city
                )
                        .isNotNull()
                        .thenGetValue();

        this.regionIso31662Code =
                guardThat(
                        "regionIso31662Code",
                        regionIso31662Code
                )
                        .isNotNull()
                        .thenGetValue();

        this.postalCode =
                guardThat(
                        "postalCode",
                        postalCode
                )
                        .isNotNull()
                        .thenGetValue();

        this.countryIso31661Alpha2Code =
                guardThat(
                        "countryIso31661Alpha2Code",
                        countryIso31661Alpha2Code
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
	public StreetAddress getAddressLine1() {
		return addressLine1;
	}

	@Override
	public AddressLine getAddressLine2() {
		return addressLine2;
	}

    @Override
    public CityName getCity() {
        return city;
    }

    @Override
    public Iso31662Code getRegionIso31662Code() {
        return regionIso31662Code;
    }

    @Override
    public PostalCode getPostalCode() {
        return postalCode;
    }

    @Override
    public Iso31661Alpha2Code getCountryIso31661Alpha2Code() {
        return countryIso31661Alpha2Code;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConsumerPostalAddressImpl address = (ConsumerPostalAddressImpl) o;

        if (!city.equals(address.city)) return false;
        if (!countryIso31661Alpha2Code.equals(address.countryIso31661Alpha2Code)) return false;
        if (!addressLine1.equals(address.addressLine1)) return false;
        if (!addressLine2.equals(address.addressLine2)) return false;
        if (!postalCode.equals(address.postalCode)) return false;
        if (!regionIso31662Code.equals(address.regionIso31662Code)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = addressLine1.hashCode();
        result = 31 * result + addressLine2.hashCode();
        result = 31 * result + city.hashCode();
        result = 31 * result + regionIso31662Code.hashCode();
        result = 31 * result + postalCode.hashCode();
        result = 31 * result + countryIso31661Alpha2Code.hashCode();
        return result;
    }

	
}
