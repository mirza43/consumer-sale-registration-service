package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

public class BuyingGroupIdImpl implements
        BuyingGroupId {

    /*
    fields
     */
    private final String buyingGroupId;

    /*
    constructors
     */
    public BuyingGroupIdImpl(
            @NonNull String buyingGroupId
    ) throws IllegalArgumentException {

        this.buyingGroupId = buyingGroupId;

    }

    /*
    getter methods
     */
    @Override
    public String getValue() {
        return buyingGroupId;
    }

    /*
    equality methods
    */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
			return true;
		}
        if (o == null || getClass() != o.getClass()) {
			return false;
		}

        BuyingGroupIdImpl that = (BuyingGroupIdImpl) o;

        return buyingGroupId.equals(that.buyingGroupId);

    }

    @Override
    public int hashCode() {
        return buyingGroupId.hashCode();
    }
}
