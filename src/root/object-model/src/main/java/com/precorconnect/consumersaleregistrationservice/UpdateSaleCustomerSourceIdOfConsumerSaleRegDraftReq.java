package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.CustomerSourceId;

import java.util.Optional;

public interface UpdateSaleCustomerSourceIdOfConsumerSaleRegDraftReq {

    ConsumerSaleRegDraftId getPartnerSaleRegDraftId();

    Optional<CustomerSourceId> getSaleCustomerSourceId();

}
