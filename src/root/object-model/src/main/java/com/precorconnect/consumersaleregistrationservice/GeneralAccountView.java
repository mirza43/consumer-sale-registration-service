package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountName;
import com.precorconnect.PostalAddress;

public interface GeneralAccountView extends AccountView {

    AccountName getName();

    PostalAddress getAddress();
}
