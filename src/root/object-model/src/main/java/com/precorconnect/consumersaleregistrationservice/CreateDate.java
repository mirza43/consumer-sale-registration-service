package com.precorconnect.consumersaleregistrationservice;

import java.time.Instant;

public interface CreateDate {

	Instant getValue();
}
