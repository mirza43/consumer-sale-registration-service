package com.precorconnect.consumersaleregistrationservice;


import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

public class PartnerCompositeSaleLineItemDtoImpl implements PartnerCompositeSaleLineItemDto {
	
	private Collection<PartnerSaleLineItemDto> components;
	
	public PartnerCompositeSaleLineItemDtoImpl(
			@NonNull Collection<PartnerSaleLineItemDto> components
	) {

		this.components =
        (Collection<PartnerSaleLineItemDto>) guardThat(
                "components",
                components
        )
                .isNotNull()
                .thenGetValue();
	}

	@Override
	public Collection<PartnerSaleLineItemDto> getComponents() {
		return components;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((components == null) ? 0 : components.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PartnerCompositeSaleLineItemDtoImpl other = (PartnerCompositeSaleLineItemDtoImpl) obj;
		if (components == null) {
			if (other.components != null)
				return false;
		} else if (!components.equals(other.components))
			return false;
		return true;
	}
	
	

}
