package com.precorconnect.consumersaleregistrationservice;

import java.util.Collection;

public interface PartnerCompositeSaleLineItemDto {
	
	Collection<PartnerSaleLineItemDto> getComponents();

}
