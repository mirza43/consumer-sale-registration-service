package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;

public interface UpdatePartnerSaleRegDraftReq {
	
		AccountId getPartnerAccountId();

}
