package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.CityName;
import com.precorconnect.Iso31661Alpha2Code;
import com.precorconnect.Iso31662Code;
import com.precorconnect.PostalCode;
import com.precorconnect.StreetAddress;

public interface ConsumerPostalAddress {

    StreetAddress getAddressLine1();
    
    AddressLine getAddressLine2();

    CityName getCity();

    Iso31662Code getRegionIso31662Code();

    PostalCode getPostalCode();

    Iso31661Alpha2Code getCountryIso31661Alpha2Code();

}

