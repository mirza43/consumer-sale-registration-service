package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.FirstName;
import com.precorconnect.LastName;

public class SubmitConsumerSaleRegDraftRequestDtoImpl 
		implements SubmitConsumerSaleRegDraftRequestDto {

	private ConsumerSaleRegDraftId consumerSaleRegDraftId;
	
	private SubmittedByName submittedByName;
	
	private PartnerRep partnerRepId;

	private FirstName firstName;
	
	private LastName lastName;
	
	private PersonEmail emailAddress;
	
	public SubmitConsumerSaleRegDraftRequestDtoImpl(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull SubmittedByName submittedByName,
			@Nullable PartnerRep partnerRepId,
			@Nullable FirstName firstName,
			@Nullable LastName lastName,
			@Nullable PersonEmail emailAddress
			) {
		
		this.consumerSaleRegDraftId =
		        guardThat(
		                "consumerSaleRegDraftId",
		                consumerSaleRegDraftId
		        )
		                .isNotNull()
		                .thenGetValue();
		
		this.submittedByName =
		        guardThat(
		                "submittedByName",
		                submittedByName
		        )
		                .isNotNull()
		                .thenGetValue();
		
		this.partnerRepId = partnerRepId;
		
		this.firstName = firstName;
		
		this.lastName = lastName;
		
		this.emailAddress = emailAddress;
		
	}
	
	@Override
	public ConsumerSaleRegDraftId getId() {
		return consumerSaleRegDraftId;
	}

	@Override
	public SubmittedByName getSubmittedByName() {
		return submittedByName;
	}

	@Override
	public Optional<FirstName> getFirstName() {
		return Optional.ofNullable(firstName);
	}

	@Override
	public Optional<LastName> getLastName() {
		return Optional.ofNullable(lastName);
	}
	
	@Override
	public Optional<PartnerRep> getPartnerRep() {
		return Optional.ofNullable(partnerRepId);
	}

	@Override
	public Optional<PersonEmail> getEmailAddress() {
		return Optional.ofNullable(emailAddress);
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((consumerSaleRegDraftId== null) ? 0 : consumerSaleRegDraftId.hashCode());
		result = prime * result + ((submittedByName == null) ? 0 : submittedByName.hashCode());
		result = prime * result + ((partnerRepId == null) ? 0 : partnerRepId.hashCode());
		result = prime * result + ((firstName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubmitConsumerSaleRegDraftRequestDtoImpl other = (SubmitConsumerSaleRegDraftRequestDtoImpl) obj;
		if (consumerSaleRegDraftId == null) {
			if (other.consumerSaleRegDraftId != null)
				return false;
		} else if (!consumerSaleRegDraftId.equals(other.consumerSaleRegDraftId))
			return false;
		if (submittedByName == null) {
			if (other.submittedByName != null)
				return false;
		} else if (!submittedByName.equals(other.submittedByName))
			return false;
		if (partnerRepId == null) {
			if (other.partnerRepId != null)
				return false;
		} else if (!partnerRepId.equals(other.partnerRepId))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		return true;
	}


}
