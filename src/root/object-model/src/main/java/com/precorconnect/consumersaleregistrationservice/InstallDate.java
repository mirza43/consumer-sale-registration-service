package com.precorconnect.consumersaleregistrationservice;

import java.time.Instant;

public interface InstallDate {

	Instant getValue();
}
