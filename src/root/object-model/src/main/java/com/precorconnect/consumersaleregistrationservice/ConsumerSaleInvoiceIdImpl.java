package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

public class ConsumerSaleInvoiceIdImpl implements ConsumerSaleInvoiceId {
	
	/*
    fields
     */
    private final String value;
    
    /*
    constructors
    */
    public ConsumerSaleInvoiceIdImpl(
            @NonNull String value
    ) throws IllegalArgumentException {

        this.value =
                guardThat(
                        "ConsumerSaleInvoiceId",
                        value
                )
                        .isNotNull()
                        .hasCharacterLengthGreaterThan(35)
                        .hasCharacterLengthLessThan(44)
                        .thenGetValue();

    }


	@Override
	public String getValue() {
		return value;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerSaleInvoiceIdImpl other = (ConsumerSaleInvoiceIdImpl) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	
	

}
