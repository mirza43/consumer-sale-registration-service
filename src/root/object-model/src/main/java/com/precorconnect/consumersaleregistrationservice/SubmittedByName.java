package com.precorconnect.consumersaleregistrationservice;

public interface SubmittedByName {
	
	String getValue();

}
