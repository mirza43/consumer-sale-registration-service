package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;

public interface ConsumerSaleRegDraftSynopsisView {

    ConsumerSaleRegDraftId getId();

    AccountId getPartnerAccountId();


}
