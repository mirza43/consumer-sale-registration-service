package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.PartnerSaleLineItem;

/**
 * a decorator that adds a unique identifier to
 * {@link com.precorconnect.PartnerSaleLineItem}
 * to allow external reference
 */
public interface ConsumerSaleRegDraftSaleLineItem
        extends PartnerSaleLineItem {

    ConsumerSaleRegDraftSaleLineItemId getId();

}
