package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;
import org.checkerframework.checker.nullness.qual.NonNull;

public class ConsumerSaleRegDraftSynopsisViewImpl
        extends AbstractConsumerSaleRegDraftSynopsisView
        implements ConsumerPartnerSaleRegDraftSynopsisView {

    /*
    constructors
     */
    public ConsumerSaleRegDraftSynopsisViewImpl(
            @NonNull ConsumerSaleRegDraftId id,
            @NonNull AccountId partnerAccountId
    ) {

        super(
                id,
                partnerAccountId
        );

    }
}
