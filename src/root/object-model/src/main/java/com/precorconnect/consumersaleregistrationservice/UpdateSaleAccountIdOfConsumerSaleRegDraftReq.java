package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;

public interface UpdateSaleAccountIdOfConsumerSaleRegDraftReq {

    AccountId getSaleAccountId();

    ConsumerSaleRegDraftId getConsumerSaleRegDraftId();

}
