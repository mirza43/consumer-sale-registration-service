package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.time.Instant;

import org.checkerframework.checker.nullness.qual.NonNull;

public class RegistrationDateImpl implements RegistrationDate {
	
	 /*
    fields
     */
    private final Instant value;

    /*
    constructor methods
     */
    public RegistrationDateImpl(
    		@NonNull Instant value
    		){

    	this.value =
                guardThat(
                        "RegistrationDate",
                         value
                )
                        .isNotNull()
                        .thenGetValue();

    }
    
    /*
    getter methods
     */
    
	@Override
	public Instant getValue() {
		
		return value;
	}

	/*
    equality methods
    */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistrationDateImpl other = (RegistrationDateImpl) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	

}
