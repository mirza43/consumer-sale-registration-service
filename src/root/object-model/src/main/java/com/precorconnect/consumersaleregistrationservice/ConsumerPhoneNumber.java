package com.precorconnect.consumersaleregistrationservice;

public interface ConsumerPhoneNumber {

    String getValue();

}

