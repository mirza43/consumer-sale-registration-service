package com.precorconnect.consumersaleregistrationservice;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConsumerSaleRegDraftIdImplTest {

    @Test(expected = IllegalArgumentException.class)
    public void constructor_NullValue_Throws(
    ) throws Exception {

        new ConsumerSaleRegDraftIdImpl(null);

    }

    @Test
    public void constructor_SetsValue(
    ) {

        /*
        arrange
         */
        Long expectedValue = 6L;

        /*
        act
         */

        ConsumerSaleRegDraftId objectUnderTest = new ConsumerSaleRegDraftIdImpl(expectedValue);


        /*
        assert
         */
        Long actualValue = objectUnderTest.getValue();

        assertThat(actualValue)
                .isEqualTo(expectedValue);


    }


}