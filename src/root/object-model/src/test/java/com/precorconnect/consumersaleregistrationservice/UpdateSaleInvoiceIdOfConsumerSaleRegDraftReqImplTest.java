package com.precorconnect.consumersaleregistrationservice;

import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThatThrownBy;

import org.assertj.core.api.StrictAssertions;
import org.junit.Test;

import com.precorconnect.PartnerSaleInvoiceId;

public class UpdateSaleInvoiceIdOfConsumerSaleRegDraftReqImplTest {

    /*
    fields
     */
    private final Dummy dummy =
            new Dummy();

    /*
    test methods
     */
    @Test
    public void constructor_NullConsumerSaleRegDraftId_Throws() {

        assertThatThrownBy(() ->
                new UpdateSaleInvoiceIdOfConsumerSaleRegDraftReqImpl(
                        null,
                        dummy.getPartnerSaleInvoiceId()
                )
        );

    }

    @Test
    public void constructor_SetsConsumerSaleRegDraftId() {

        /*
        arrange
         */
        ConsumerSaleRegDraftId expectedConsumerSaleRegDraftId =
                dummy.getConsumerSaleRegDraftId();

        /*
        act
         */
        UpdateSaleInvoiceIdOfConsumerSaleRegDraftReq objectUnderTest =
                new UpdateSaleInvoiceIdOfConsumerSaleRegDraftReqImpl(
                        expectedConsumerSaleRegDraftId, dummy.getPartnerSaleInvoiceId()
                );

        /*
        assert
         */
        ConsumerSaleRegDraftId actualConsumerSaleRegDraftId =
                objectUnderTest.getPartnerSaleRegDraftId();

        assertThat(actualConsumerSaleRegDraftId)
                .isEqualTo(expectedConsumerSaleRegDraftId);

    }

    @Test
    public void constructor_NullSaleInvoiceId_DoesNotThrow() {

        new UpdateSaleInvoiceIdOfConsumerSaleRegDraftReqImpl(
                dummy.getConsumerSaleRegDraftId(),
                null
        );

    }

    @Test
    public void constructor_SetsSaleInvoiceId() {

        /*
        arrange
         */
        PartnerSaleInvoiceId expectedSaleInvoiceId =
                dummy.getPartnerSaleInvoiceId();

        /*
        act
         */
        UpdateSaleInvoiceIdOfConsumerSaleRegDraftReq objectUnderTest =
                new UpdateSaleInvoiceIdOfConsumerSaleRegDraftReqImpl(
                        dummy.getConsumerSaleRegDraftId(),
                        expectedSaleInvoiceId
                );

        /*
        assert
         */
        PartnerSaleInvoiceId actualSaleInvoiceId =
        		objectUnderTest.getSaleInvoiceId().isPresent()
                ?objectUnderTest.getSaleInvoiceId().get():null;

        StrictAssertions.assertThat(actualSaleInvoiceId)
                .isEqualTo(expectedSaleInvoiceId);

    }

}