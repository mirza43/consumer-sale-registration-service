package com.precorconnect.consumersaleregistrationservice;


import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThatThrownBy;

import org.junit.Test;

import com.precorconnect.AccountId;

public class AddConsumerSaleRegDraftReqImplTest {


    //fields

    private final Dummy dummy =
            new Dummy();


   // getter methods

    @Test
    public void constructor_NullPartnerAccountId_Throws() {

        assertThatThrownBy(() ->
                new AddConsumerSaleRegDraftReqImpl(
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        null,
                        dummy.getInvoiceNumber(),
                        dummy.getInvoiceUrl(),
                        dummy.isSubmitted(),
                        dummy.getSellDate(),
                        dummy.getUserId(),
                        dummy.getSaleLineItems()
                )
        );

    }

    @Test
    public void constructor_SetsPartnerPartnerAccountId() {


      //  arrange

        AccountId expectedPartnerAccountId =
                dummy.getAccountId();


       // act

        AddConsumerSaleRegDraftReq objectUnderTest =
                new AddConsumerSaleRegDraftReqImpl(
                		dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getPostalAddress(),
                        dummy.getPhoneNumber(),
                        dummy.getPersonEmail(),
                        expectedPartnerAccountId,
                        dummy.getInvoiceNumber(),
                        dummy.getInvoiceUrl(),
                        dummy.isSubmitted(),
                        dummy.getSellDate(),
                        dummy.getUserId(),
                        dummy.getSaleLineItems()
                );


       // assert

        AccountId actualPartnerAccountId =
                objectUnderTest.getPartnerAccountId();

        assertThat(actualPartnerAccountId)
                .isEqualTo(expectedPartnerAccountId);

    }

}