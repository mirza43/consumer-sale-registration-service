package com.precorconnect.consumersaleregistrationservice.webapi;

import java.util.Optional;


public class ProductSaleRegistrationRuleEngineWebDto {
	
	private Long registrationId;

	private String sellDate;

	private String installDate;

	private String submittedDate;
	
	private String facilityBrand;

	private Iterable<PartnerSaleSimpleLineItemWebDto> simpleLineItems;
	
	private Iterable<PartnerSaleCompositeLineItemWebDto> compositeLineItems;
	
	public Optional<Long> getRegistrationId() {
		return Optional.ofNullable(registrationId);
	}

	public void setRegistrationId(Long registrationId) {
		this.registrationId = registrationId;
	}

	public String getSellDate() {
		return sellDate;
	}

	public void setSellDate(String sellDate) {
		this.sellDate = sellDate;
	}

	public Optional<String> getInstallDate() {
		return Optional.ofNullable(installDate);
	}

	public void setInstallDate(String installDate) {
		this.installDate = installDate;
	}

	public Optional<String> getSubmittedDate() {
		return Optional.ofNullable(submittedDate);
	}

	public void setSubmittedDate(String submittedDate) {
		this.submittedDate = submittedDate;
	}

	public String getFacilityBrand() {
		return facilityBrand;
	}

	public void setFacilityBrand(String facilityBrand) {
		this.facilityBrand = facilityBrand;
	}

	public Optional<Iterable<PartnerSaleSimpleLineItemWebDto>> getSimpleLineItems() {
		return Optional.ofNullable(simpleLineItems);
	}

	public void setSimpleLineItems(
			Iterable<PartnerSaleSimpleLineItemWebDto> simpleLineItems) {
		this.simpleLineItems = simpleLineItems;
	}

	public Optional<Iterable<PartnerSaleCompositeLineItemWebDto>> getCompositeLineItems() {
		return Optional.ofNullable(compositeLineItems);
	}

	public void setCompositeLineItems(
			Iterable<PartnerSaleCompositeLineItemWebDto> compositeLineItems) {
		this.compositeLineItems = compositeLineItems;
	}

	@Override
	public String toString() {
		return "ProductSaleRegistrationRuleEngineWebDto [registrationId="
				+ registrationId + ", sellDate=" + sellDate + ", installDate="
				+ installDate + ", submittedDate=" + submittedDate
				+ ", facilityBrand=" + facilityBrand + ", simpleLineItems="
				+ simpleLineItems + ", compositeLineItems="
				+ compositeLineItems + "]";
	}	

}
