package com.precorconnect.consumersaleregistrationservice.webapi;

import java.util.Optional;

public class SubmitConsumerSaleRegDraftDto {
	
	private Long id;
	
	private String submittedByName;

	private String partnerRepId;
	
	private String firstName;
	
	private String lastName;
	
	private String emailAddress;

	public Long getId() {
		return id;
	}

	public String getSubmittedByName() {
		return submittedByName;
	}

	public Optional<String> getPartnerRepId() {
		return Optional.ofNullable(partnerRepId);
	}
	
	public Optional<String> getFirstName() {
		return Optional.ofNullable(firstName);
	}

	public Optional<String> getEmailAddress() {
		return Optional.ofNullable(emailAddress);
	}

	public Optional<String> getLastName() {
		return Optional.ofNullable(lastName);
	}
	

}
