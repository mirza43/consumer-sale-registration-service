package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;


public class UpdateConsumerSaleRegDraftSaleCompositeLineItem {
	
	private Long id;

	private Collection<ConsumerSaleLineItemComponent> components;

	private BigDecimal price;

	public UpdateConsumerSaleRegDraftSaleCompositeLineItem(
			@NonNull Long id,
			@NonNull List<ConsumerSaleLineItemComponent> components,
			@Nullable BigDecimal price
	) {
		
		this.id=id;

		this.components =
                (Collection<ConsumerSaleLineItemComponent>) guardThat(
                        "components",
                        components
                		)
                        .isNotNull()
                        .thenGetValue();
		
		this.price=price;
		
		
	}
	
	

	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public Collection<ConsumerSaleLineItemComponent> getComponents() {
		return components;
	}

	public void setComponents(Collection<ConsumerSaleLineItemComponent> components) {
		this.components = components;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
}
