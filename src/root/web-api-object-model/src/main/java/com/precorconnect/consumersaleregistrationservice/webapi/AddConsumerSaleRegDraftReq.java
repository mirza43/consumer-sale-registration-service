package com.precorconnect.consumersaleregistrationservice.webapi;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Optional;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;





public class AddConsumerSaleRegDraftReq {


	/*
    fields
     */
	private final String firstName;
	    
    private final String lastName;

    private final PostalAddress address;

    private final String phoneNumber;
    
    private final String personEmail;
    
    private final String invoiceNumber;
    
    private final String sellDate;

    private final String partnerRepUserId;
    
    private final String partnerAccountId;

    private final String invoiceUrl;

    private final boolean isSubmitted;

	private final Iterable<ConsumerSaleRegDraftSaleSimpleLineItem> simpleLineItems;

    private final Iterable<ConsumerSaleRegDraftSaleCompositeLineItem> compositeLineItems;
    

	public AddConsumerSaleRegDraftReq(
			@NonNull String firstName,
            @NonNull String lastName,
            @NonNull PostalAddress address,
            @Nullable String phoneNumber,
            @Nullable String personEmail,
			@NonNull String sellDate, 
			@NonNull String invoiceNumber, 
			@Nullable String partnerRepUserId,
			@NonNull String partnerAccountId,
			@Nullable String invoiceUrl,
			@NonNull boolean isSubmitted, 
			@Nullable Iterable<ConsumerSaleRegDraftSaleSimpleLineItem> simpleLineItems,
			@Nullable Iterable<ConsumerSaleRegDraftSaleCompositeLineItem> compositeLineItems
			) {
		
		 this.firstName = 
	 				guardThat(
	 						"fisrtName",
	 						firstName
	 				)
	 						.isNotNull()
	 						.thenGetValue();
	    	 
	    	 this.lastName = 
	  				guardThat(
	  						"lastName",
	  						lastName
	  				)
	  						.isNotNull()
	  						.thenGetValue();

	         this.address = 
	 				guardThat(
	 						"address",
	 						address
	 				)
	 						.isNotNull()
	 						.thenGetValue();

	         this.phoneNumber = phoneNumber; 
	 				
	         
	         this.personEmail=personEmail;
	         
	         this.invoiceNumber =
	                 guardThat(
	                         "invoiceNumber",
	                         invoiceNumber
	                 		)
	                         .isNotNull()
	                         .thenGetValue();
	         
	         this.sellDate =
	                 guardThat(
	                         "sellDate",
	                         sellDate
	                 		)
	                         .isNotNull()
	                         .thenGetValue();
		
		this.isSubmitted = 
				guardThat(
                        "isSubmitted",
                        isSubmitted
                		)
                        .isNotNull()
                        .thenGetValue();
		
		this.partnerAccountId = 
				guardThat(
	                "partnerAccountId",
	                partnerAccountId
	        		)
	                .isNotNull()
	                .thenGetValue();

		
		this.partnerRepUserId = partnerRepUserId;
		
		this.invoiceUrl = invoiceUrl;
		
		this.simpleLineItems = simpleLineItems;
		
		this.compositeLineItems = compositeLineItems;
	}
	
	/**
	 * getter methods
	 */


	public String getFirstName() {
		return firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public PostalAddress getAddress() {
		return address;
	}


	public Optional<String> getPhoneNumber() {
		return Optional.ofNullable(phoneNumber);
	}


	public Optional<String> getPersonEmail() {
		return Optional.ofNullable(personEmail);
	}


	public Optional<String>  getPartnerRepUserId() {
		return Optional.ofNullable(partnerRepUserId);
	}


	public String getPartnerAccountId() {
		return partnerAccountId;
	}


	public Optional<String> getInvoiceUrl() {
		return Optional.ofNullable(invoiceUrl);
	}


	public boolean isSubmitted() {
		return isSubmitted;
	}


	public Optional<Iterable<ConsumerSaleRegDraftSaleSimpleLineItem>> getSimpleLineItems() {
		return Optional.ofNullable(simpleLineItems);
	}


	public Optional<Iterable<ConsumerSaleRegDraftSaleCompositeLineItem>> getCompositeLineItems() {
		return Optional.ofNullable(compositeLineItems);
	}
	
	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public String getSellDate() {
		return sellDate;
	}
	/**
	    * equality methods
	    */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((compositeLineItems == null) ? 0 : compositeLineItems.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((invoiceNumber == null) ? 0 : invoiceNumber.hashCode());
		result = prime * result + ((invoiceUrl == null) ? 0 : invoiceUrl.hashCode());
		result = prime * result + (isSubmitted ? 1231 : 1237);
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((partnerAccountId == null) ? 0 : partnerAccountId.hashCode());
		result = prime * result + ((partnerRepUserId == null) ? 0 : partnerRepUserId.hashCode());
		result = prime * result + ((personEmail == null) ? 0 : personEmail.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((sellDate == null) ? 0 : sellDate.hashCode());
		result = prime * result + ((simpleLineItems == null) ? 0 : simpleLineItems.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AddConsumerSaleRegDraftReq other = (AddConsumerSaleRegDraftReq) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (compositeLineItems == null) {
			if (other.compositeLineItems != null)
				return false;
		} else if (!compositeLineItems.equals(other.compositeLineItems))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (invoiceNumber == null) {
			if (other.invoiceNumber != null)
				return false;
		} else if (!invoiceNumber.equals(other.invoiceNumber))
			return false;
		if (invoiceUrl == null) {
			if (other.invoiceUrl != null)
				return false;
		} else if (!invoiceUrl.equals(other.invoiceUrl))
			return false;
		if (isSubmitted != other.isSubmitted)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (partnerAccountId == null) {
			if (other.partnerAccountId != null)
				return false;
		} else if (!partnerAccountId.equals(other.partnerAccountId))
			return false;
		if (partnerRepUserId == null) {
			if (other.partnerRepUserId != null)
				return false;
		} else if (!partnerRepUserId.equals(other.partnerRepUserId))
			return false;
		if (personEmail == null) {
			if (other.personEmail != null)
				return false;
		} else if (!personEmail.equals(other.personEmail))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (sellDate == null) {
			if (other.sellDate != null)
				return false;
		} else if (!sellDate.equals(other.sellDate))
			return false;
		if (simpleLineItems == null) {
			if (other.simpleLineItems != null)
				return false;
		} else if (!simpleLineItems.equals(other.simpleLineItems))
			return false;
		return true;
	}

}
