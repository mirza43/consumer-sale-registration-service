Feature: Update Sale Invoice URL Consumer Sale Registration Service
	
	Updates Sale Invoice URL Consumer Sale Registration Service
	
	Background:
	Given an updateSaleInvoiceUrlOfConsumerSaleRegDraft has attributes
	  | attribute              | validation | type   |
      | consumerSaleRegDraftId | required   | long   |
      | invoiceUrl             | required   | string |	
      
   Scenario: Success
   Given consumerSaleRegDraftId is the id of an existing consumerSaleRegDraftId
   And I provide an invoiceUrl to existing consumerSaleRegDraftId
   And invoiceUrl is represented in the Database by dbUpdateSaleInvoiceUrlOfConsumerSaleRegDraft.invoiceURL
   And I provide an accessToken identifying me as a consumer rep associated with consumerSaleRegDraftId.partnerAccountId
   When I execute updateSaleInvoiceUrlOfConsumerSaleRegDraft
   Then dbUpdateSaleInvoiceUrlOfConsumerSaleRegDraft.invoiceUrl is updated to updateSaleInvoiceUrlOfConsumerSaleRegDraft.invoiceURL
   