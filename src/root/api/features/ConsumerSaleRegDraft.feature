Feature: Consumer Sale Registration Draft details

	Adds Consumer Sale Registration Draft details to database
	
	Background:
	  Given AddConsumerSaleRegDraftReq has Attributes
	  | attribute              | validation | type         |
      | firstName              | required   | string       |
      | lastName               | required   | string       |
      | PostalAddress          | required   | address      |
      | phoneNumber            | required   | string       |
      | personEmail            | required   | string       |
      | invoiceNumber          | required   | string       |
      | sellDate               | required   | string       |
      | partnerRepUserId       | required   | string       |
      | partnerAccountId       | required   | string       |
      | invoiceUrl             | required   | string       |
      | isSubmitted            | required   | boolean      |
      | simpleLineItems        | required   | collection   |
      | compositeLineItems     | required   | collection   |
      
      
 Scenario: Success
   Given new AddConsumerSaleRegDraftReq object for inserting consumer sale registration details to database
   And I provide an accessToken identifying me as a consumer rep associated with ConsumerSaleRegDrafts.partnerAccountId
   When I execute AddConsumerSaleRegDraftReq
   Then AddConsumerSaleRegDraftReq record details will be inserted into ConsumerPartnerSaleRegDrafts and ConsumerSaleRegDrafts tables   
   When I execute getConsumerSaleRegDraft
   Then consumerSaleRegDraft is returned
   When I execute UpdateConsumerSaleRegDraftReq
   Then UpdateConsumerSaleRegDraftReq record details will be updated into ConsumerPartnerSaleRegDrafts and ConsumerSaleRegDrafts tables  
    When I execute SubmitConsumerSaleRegDraft
   Then dbConsumerSaleRegDraft.isSubmitted is updated to true
   When I execute deleteConsumerSaleRegDraftWithId
   Then ConsumerSaleRegDrafts.Id record will be deleted from ConsumerSaleRegDrafts table        