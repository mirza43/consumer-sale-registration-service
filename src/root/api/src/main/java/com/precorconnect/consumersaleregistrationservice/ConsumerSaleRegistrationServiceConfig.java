package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.consumersaleregistrationservice.claimspiffservice.ClaimSpiffServiceAdapterConfig;
import com.precorconnect.consumersaleregistrationservice.database.DatabaseAdapterConfig;
import com.precorconnect.consumersaleregistrationservice.identityservice.IdentityServiceAdapterConfig;
import com.precorconnect.consumersaleregistrationservice.masterdataservice.MasterDataServiceAdapterConfig;
import com.precorconnect.consumersaleregistrationservice.partnerrepservice.PartnerRepServiceAdapterConfig;
import com.precorconnect.partnersaleregdraftservice.registrationlogservice.RegistrationLogServiceAdapterConfig;

public interface ConsumerSaleRegistrationServiceConfig {
	
	IdentityServiceAdapterConfig getIdentityServiceAdapterConfig();

    DatabaseAdapterConfig getDatabaseAdapterConfig();

    ClaimSpiffServiceAdapterConfig getSpiffEntitlementServiceAdapterConfig();

    MasterDataServiceAdapterConfig getMasterDataServiceAdapterConfig();
    
    RegistrationLogServiceAdapterConfig getRegistrationLogServiceAdapterConfig();
    
    PartnerRepServiceAdapterConfig getPartnerRepServiceAdapterConfig();

}
