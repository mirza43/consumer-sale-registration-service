package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.AccountId;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.identityservice.AppJwt;
import com.precorconnect.identityservice.AppJwtImpl;
import com.precorconnect.identityservice.PartnerRepJwt;
import com.precorconnect.identityservice.PartnerRepJwtImpl;
import com.precorconnect.identityservice.integrationtestsdk.IdentityServiceIntegrationTestSdk;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.time.Duration;
import java.time.Instant;

import static com.precorconnect.guardclauses.Guards.guardThat;

public class AccessTokenFactory {

    /*
    fields
     */
    private final Dummy dummy;

    private final IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk;

    /*
       constructors
        */
    public AccessTokenFactory(
            @NonNull Dummy dummy,
            @NonNull IdentityServiceIntegrationTestSdk identityServiceIntegrationTestSdk
    ) {

        this.dummy =
                guardThat(
                        "dummy",
                        dummy
                )
                        .isNotNull()
                        .thenGetValue();

        this.identityServiceIntegrationTestSdk =
                guardThat(
                        "identityServiceIntegrationTestSdk",
                        identityServiceIntegrationTestSdk
                )
                        .isNotNull()
                        .thenGetValue();
			
    }

    /*
    factory methods
     */
    public OAuth2AccessToken constructValidAppOAuth2AccessToken() {

        AppJwt appJwt =
                new AppJwtImpl(
                        Instant.now().plusSeconds(480),
                        dummy.getUri(),
                        dummy.getUri()
                );

        return
                identityServiceIntegrationTestSdk
                        .getAppOAuth2AccessToken(appJwt);
    }

    public OAuth2AccessToken constructValidPartnerRepOAuth2AccessToken(
    ) {
        return constructValidPartnerRepOAuth2AccessToken(
                dummy.getAccountId()
        );
    }

    public OAuth2AccessToken constructValidPartnerRepOAuth2AccessToken(
            @NonNull AccountId accountId
    ) {

        PartnerRepJwt partnerRepJwt =
                new PartnerRepJwtImpl(
                        Instant.now().plus(Duration.ofMinutes(10)),
                        dummy.getUri(),
                        dummy.getUri(),
                        dummy.getFirstName(),
                        dummy.getLastName(),
                        dummy.getUserId(),
                        accountId,
                        dummy.getSapVendorNumber()
                );

        return identityServiceIntegrationTestSdk
                .getPartnerRepOAuth2AccessToken(partnerRepJwt);

    }

}
