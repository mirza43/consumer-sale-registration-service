package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.identityservice.HmacKey;
import com.precorconnect.identityservice.HmacKeyImpl;

public class ConfigFactory {
	
	public Config construct() {

		String keyValue = System.getenv("TEST_IDENTITY_SERVICE_JWT_SIGNING_KEY");

		HmacKey key = new HmacKeyImpl(keyValue);

		return new Config(
				key,
				new ConsumerSaleRegistrationServiceConfigFactoryImpl()
					.construct()
			);
	}

}
