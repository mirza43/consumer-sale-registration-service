package com.precorconnect.consumersaleregistrationservice;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features={"features/ConsumerSaleRegDraft.feature"},
		glue = {"com.precorconnect.consumersaleregistrationservice"}
)
public class ConsumerSaleRegDraftIT {

}
