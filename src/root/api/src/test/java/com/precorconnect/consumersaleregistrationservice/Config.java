package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.identityservice.HmacKey;

public class Config {
	
	private final HmacKey identityServiceJwtSigningKey;

    private final ConsumerSaleRegistrationServiceConfig consumerSaleRegistrationServiceConfig;

    public Config(
            @NonNull HmacKey identityServiceJwtSigningKey,
            @NonNull ConsumerSaleRegistrationServiceConfig consumerSaleRegistrationServiceConfig
    ) {

        if (null == identityServiceJwtSigningKey) {
            throw new IllegalArgumentException(
                    String.format(
                            "%s => identityServiceJwtSigningKey cannot be null",
                            getClass().getCanonicalName()
                    )
            );
        }
        this.identityServiceJwtSigningKey = identityServiceJwtSigningKey;

        if (null == consumerSaleRegistrationServiceConfig) {
            throw new IllegalArgumentException(
                    String.format(
                            "%s => consumerSaleRegistrationServiceConfig cannot be null",
                            getClass().getCanonicalName()
                    )
            );
        }
        this.consumerSaleRegistrationServiceConfig = consumerSaleRegistrationServiceConfig;

    }

    
	public HmacKey getIdentityServiceJwtSigningKey() {
		return identityServiceJwtSigningKey;
	}

	public ConsumerSaleRegistrationServiceConfig getConsumerSaleRegistrationServiceConfig() {
		return consumerSaleRegistrationServiceConfig;
	}
    
}
