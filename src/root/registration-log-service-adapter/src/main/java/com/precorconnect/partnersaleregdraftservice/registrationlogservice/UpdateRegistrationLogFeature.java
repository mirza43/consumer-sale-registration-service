package com.precorconnect.partnersaleregdraftservice.registrationlogservice;

import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;

public interface UpdateRegistrationLogFeature {
	
	void updateRegistrationLog(
			@NonNull List<UpdateRegistrationLog> request,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException;	

}
