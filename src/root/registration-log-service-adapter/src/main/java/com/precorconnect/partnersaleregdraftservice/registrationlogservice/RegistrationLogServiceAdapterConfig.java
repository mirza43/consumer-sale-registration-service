package com.precorconnect.partnersaleregdraftservice.registrationlogservice;

import java.net.URL;

public interface RegistrationLogServiceAdapterConfig {

	URL getPrecorConnectApiBaseUrl();
}
