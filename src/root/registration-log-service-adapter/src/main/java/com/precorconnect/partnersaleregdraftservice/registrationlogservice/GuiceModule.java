package com.precorconnect.partnersaleregdraftservice.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.precorconnect.registrationlogservice.sdk.RegistrationLogServiceSdk;
import com.precorconnect.registrationlogservice.sdk.RegistrationLogServiceSdkConfig;
import com.precorconnect.registrationlogservice.sdk.RegistrationLogServiceSdkConfigImpl;
import com.precorconnect.registrationlogservice.sdk.RegistrationLogServiceSdkImpl;


class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final RegistrationLogServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull RegistrationLogServiceAdapterConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();
    }

    @Override
    protected void configure() {

        bind(RegistrationLogServiceSdkConfig.class)
                .toInstance(
                        new RegistrationLogServiceSdkConfigImpl(
                                config.getPrecorConnectApiBaseUrl()
                        )
                );

        bind(RegistrationLogServiceSdk.class)
                .to(RegistrationLogServiceSdkImpl.class)
                .in(Singleton.class);

        bindFeatures();

    }

    private void bindFeatures() {

        bind(AddRegistrationLogFeature.class)
                .to(AddRegistrationLogFeatureImpl.class)
                .in(Singleton.class);
        
        bind(UpdateRegistrationLogFeature.class)
        		.to(UpdateRegistrationLogFeatureImpl.class)
        		.in(Singleton.class);

    }

}
