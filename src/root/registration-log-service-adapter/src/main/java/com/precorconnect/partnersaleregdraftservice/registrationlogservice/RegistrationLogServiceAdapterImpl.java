package com.precorconnect.partnersaleregdraftservice.registrationlogservice;

import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.RegistrationLogServiceAdapter;
import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;

@Singleton
public class RegistrationLogServiceAdapterImpl implements
		RegistrationLogServiceAdapter {

	/*
	fields
	 */
	private final Injector injector;
	
	/*
	constructors
	 */
	public RegistrationLogServiceAdapterImpl(
	        @NonNull RegistrationLogServiceAdapterConfig config
	) {
	
	    GuiceModule guiceModule =
	            new GuiceModule(config);
	
	    injector =
	            Guice.createInjector(guiceModule);
	}
	
	@Override
	public Long addRegistrationLog(
				@NonNull AddRegistrationLog request,
				@NonNull OAuth2AccessToken accessToken
				) throws AuthenticationException {
	
	    return
	            injector
	            	.getInstance(AddRegistrationLogFeature.class)
	                .addRegistrationLog(request,accessToken);
	
	}
	
	@Override
	public void updateRegistrationLog(
			@NonNull List<UpdateRegistrationLog> request, 
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {
	
		 injector
	     	.getInstance(UpdateRegistrationLogFeature.class)
	     	.updateRegistrationLog(request,accessToken);
	
		}

}



