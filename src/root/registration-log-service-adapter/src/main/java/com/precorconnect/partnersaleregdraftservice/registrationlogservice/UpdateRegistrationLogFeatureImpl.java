package com.precorconnect.partnersaleregdraftservice.registrationlogservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.registrationlogservice.sdk.RegistrationLogServiceSdk;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;

@Singleton
public class UpdateRegistrationLogFeatureImpl 
				implements UpdateRegistrationLogFeature {


	/*
    fields
     */
    private final RegistrationLogServiceSdk registrationLogServiceSdk;

    /*
    constructors
     */
    @Inject
    public UpdateRegistrationLogFeatureImpl(
            @NonNull RegistrationLogServiceSdk registrationLogServiceSdk
    ) {

    	this.registrationLogServiceSdk =
                guardThat(
                        "registrationLogServiceSdk",
                        registrationLogServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
	public void updateRegistrationLog(
			@NonNull List<UpdateRegistrationLog> request, 
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {

        registrationLogServiceSdk
                .updateRegistrationLog(
                		request,
                        accessToken
                );

    }
}
