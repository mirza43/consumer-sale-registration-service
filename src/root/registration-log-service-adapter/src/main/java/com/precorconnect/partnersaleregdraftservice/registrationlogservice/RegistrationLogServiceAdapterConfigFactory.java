package com.precorconnect.partnersaleregdraftservice.registrationlogservice;

public interface RegistrationLogServiceAdapterConfigFactory {

	RegistrationLogServiceAdapterConfig construct();

}
