package com.precorconnect.consumersaleregistrationservice;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleCompositeLineItemWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleSimpleLineItemWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

public class GetSpiffAmountRequestFactoryImpl implements GetSpiffAmountRequestFactory {


	private final ConsumerSaleLineItemWebDtoFactory consumerSaleLineItemWebDtoFactory
				= new ConsumerSaleLineItemWebDtoFactoryImpl();
	@Override
	public ProductSaleRegistrationRuleEngineWebDto construct(
			@NonNull ConsumerPartnerSaleRegDraftView consumerCommercialSaleRegDraftView,
			Collection<SerialNumber> serialNumberList) {


		ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto
				= new ProductSaleRegistrationRuleEngineWebDto();

		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");

		

		String sellDate = format.format(
								Date.from(consumerCommercialSaleRegDraftView
										.getSellDate())
								);

		String submittedDate = format.format(
									Date.from(consumerCommercialSaleRegDraftView
										.getSaleCreatedAtTimestamp())
									);

		productSaleRegistrationRuleEngineWebDto
					.setSellDate(sellDate);
		
		
		productSaleRegistrationRuleEngineWebDto
					.setInstallDate(sellDate);

		

		productSaleRegistrationRuleEngineWebDto
					.setSubmittedDate(submittedDate);

			

			productSaleRegistrationRuleEngineWebDto
					.setRegistrationId(
							consumerCommercialSaleRegDraftView
								.getId()
								.getValue()
							);


			List<PartnerSaleSimpleLineItemWebDto> simpleLineItems = new ArrayList<PartnerSaleSimpleLineItemWebDto>();


			List<PartnerSaleCompositeLineItemWebDto> compositeLineItems = new ArrayList<PartnerSaleCompositeLineItemWebDto>();


			for(ConsumerSaleRegDraftSaleLineItem lineItem:  consumerCommercialSaleRegDraftView.getSaleLineItems()){

		    	 if(lineItem instanceof com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem){
		    		 

	     		    	SerialNumber serialNumber = 
	     		    			new SerialNumberImpl(
	     		    						((com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem) lineItem)
	     		    						.getSerialNumber()
	     		    						.getValue()
	     		    						);
	     		    	
	     		    	if(!serialNumberList.contains(serialNumber)){
		    		 simpleLineItems.add(
		    			consumerSaleLineItemWebDtoFactory
								.construct(
										(com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem)lineItem
								)
		    				 );
	     		    	}
		    	 }else if(lineItem instanceof com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem){
		    		 
		    		 boolean flag=false;
		    		 
		    		 Collection<ConsumerSaleLineItemComponent> consumerSaleLineItemComponentList = 
  		    										(Collection<ConsumerSaleLineItemComponent>) 
  		    										((com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem)lineItem)
  		    										.getComponents();
  		    	
  		    	for(ConsumerSaleLineItemComponent consumerSaleLineItemComponent:consumerSaleLineItemComponentList){
  		    		
  		    		SerialNumber serialNumber = 
  		    				new SerialNumberImpl(
  		    						consumerSaleLineItemComponent
  		    							.getSerialNumber()
  		    							.getValue()
  		    							);
  		    		
  		    		if(serialNumberList.contains(serialNumber)){
  		    			flag=true;
  		        	}
  		    		
  		    	}
  		    		if(!flag){
		    		 compositeLineItems.add(
		    			consumerSaleLineItemWebDtoFactory
	    				 	.construct(
	    				 			(com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem)lineItem
	    				 	)
	    				 );
  		    		}

		    	 }


		     }

			productSaleRegistrationRuleEngineWebDto
					.setSimpleLineItems(simpleLineItems);

			productSaleRegistrationRuleEngineWebDto
					.setCompositeLineItems(compositeLineItems);
		return productSaleRegistrationRuleEngineWebDto;
	}

}
