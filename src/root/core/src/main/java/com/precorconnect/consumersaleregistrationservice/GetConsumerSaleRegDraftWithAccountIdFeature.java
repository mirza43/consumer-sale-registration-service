package com.precorconnect.consumersaleregistrationservice;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.OAuth2AccessToken;

public interface GetConsumerSaleRegDraftWithAccountIdFeature {

	Collection<ConsumerPartnerSaleRegDraftView> execute(
            @NonNull AccountId accountId,
            @NonNull OAuth2AccessToken accessToken
    );

}
