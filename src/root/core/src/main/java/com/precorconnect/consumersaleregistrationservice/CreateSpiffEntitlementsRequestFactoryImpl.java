package com.precorconnect.consumersaleregistrationservice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.AccountIdImpl;
import com.precorconnect.UserId;
import com.precorconnect.UserIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.FacilityName;
import com.precorconnect.claimspiffservice.objectmodel.FacilityNameImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumber;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceNumberImpl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrl;
import com.precorconnect.claimspiffservice.objectmodel.InvoiceUrlImpl;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationId;
import com.precorconnect.claimspiffservice.objectmodel.PartnerSaleRegistrationIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.SellDate;
import com.precorconnect.claimspiffservice.objectmodel.SellDateImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmount;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDtoImpl;

public class CreateSpiffEntitlementsRequestFactoryImpl implements
		CreateSpiffEntitlementsRequestFactory {

	@Override
	public SpiffEntitlementDto construct(
				@NonNull ConsumerPartnerSaleRegDraftView consumerCommercialSaleRegDraftView,
				@NonNull SpiffAmount spiffAmount) {

		AccountId accountId = new
									AccountIdImpl(
											consumerCommercialSaleRegDraftView
												.getPartnerAccountId()
												.getValue()
										);


		PartnerSaleRegistrationId partnerSaleRegistrationId = new
																PartnerSaleRegistrationIdImpl(
																		consumerCommercialSaleRegDraftView
																			.getId()
																			.getValue()
																	);

	

		InvoiceNumber invoiceNumber = new
										InvoiceNumberImpl(
												consumerCommercialSaleRegDraftView
        											.getInvoiceNumber()
        											.getValue()
        										);


		InvoiceUrl invoiceUrl = consumerCommercialSaleRegDraftView
    										.getInvoiceUrl().isPresent()
	    									?new
	    											InvoiceUrlImpl(
	    													consumerCommercialSaleRegDraftView
	    														.getInvoiceUrl().get().getValue()
	        										)
	        								:null;


		UserId partnerRepUserId = consumerCommercialSaleRegDraftView
											.getSalePartnerRepId().isPresent()
											?new
													UserIdImpl(
															consumerCommercialSaleRegDraftView
																.getSalePartnerRepId().get().getValue()
													)
											:null;

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		String date;

		Date formattedSellDate;
		try {

			date = formatter.format(Date.from(consumerCommercialSaleRegDraftView
										.getSellDate())
								);

			formattedSellDate = formatter.parse(date);

		} catch (ParseException e) {

			throw new RuntimeException("Date field parse exception: ",e);

		}


		SellDate sellDate = new
								SellDateImpl(
										formattedSellDate
										);
		
		 FacilityName facilityName = new FacilityNameImpl(consumerCommercialSaleRegDraftView.getFirstName().getValue() +" "+ 
				 consumerCommercialSaleRegDraftView.getLastName().getValue());


        return
                new SpiffEntitlementDtoImpl(
                		accountId,
                		partnerSaleRegistrationId,
                		facilityName,
                		invoiceNumber,
                		invoiceUrl,
                		partnerRepUserId,
                		null,
                		spiffAmount,
                		sellDate
                );

	}

}
