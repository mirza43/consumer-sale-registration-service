package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;

public interface IsEligibleForSpiffFeature {
	
	boolean execute(
			@NonNull ProductSaleRegistrationRuleEngineRequestDto productSaleRegistrationRuleEngineRequestDto,
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException, AuthorizationException;

}
