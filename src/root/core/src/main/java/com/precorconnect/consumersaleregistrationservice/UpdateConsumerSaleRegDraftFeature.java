package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2AccessToken;

public interface UpdateConsumerSaleRegDraftFeature {

	ConsumerPartnerSaleRegDraftView execute(
	            @NonNull UpdateConsumerSaleRegDraftReq request,
	            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
	            @NonNull OAuth2AccessToken accessToken
	    );

}
