package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2AccessToken;

@Singleton
class UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeatureImpl
        implements UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public void execute(
            @NonNull UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request,
            @NonNull OAuth2AccessToken accessToken
    ) {

        databaseAdapter
		                  .updateSaleInvoiceUrlOfConsumerSaleRegDraft(
		                        request
		                 );

    }
}
