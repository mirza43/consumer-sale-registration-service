package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2AccessToken;


@Singleton
class ListConsumerSaleRegDraftWithIdsFeatureImpl
        implements ListConsumerSaleRegDraftWithIdsFeature {

	/*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public ListConsumerSaleRegDraftWithIdsFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<ConsumerPartnerSaleRegDraftView> execute(
			@NonNull Collection<ConsumerSaleRegDraftId> consumerSaleRegDraftIds,
			@NonNull OAuth2AccessToken accessToken
			) {

		 return
	                databaseAdapter
	                        .listConsumerSaleRegDraftsWithIds(
	                        		consumerSaleRegDraftIds
	                        );
	}

}
