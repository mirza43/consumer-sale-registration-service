package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

interface RemoveSaleLineItemFromConsumerSaleRegDraftFeature {

    void execute(
            @NonNull ConsumerSaleRegDraftSaleLineItemId consumerSaleRegDraftSaleLineItemId,
            @NonNull OAuth2AccessToken accessToken
    );

}
