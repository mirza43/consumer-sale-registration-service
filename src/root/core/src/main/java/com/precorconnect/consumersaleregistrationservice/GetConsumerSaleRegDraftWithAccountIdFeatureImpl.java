package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.OAuth2AccessToken;

@Singleton
public class GetConsumerSaleRegDraftWithAccountIdFeatureImpl implements
		GetConsumerSaleRegDraftWithAccountIdFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public GetConsumerSaleRegDraftWithAccountIdFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public Collection<ConsumerPartnerSaleRegDraftView> execute(
			@NonNull AccountId accountId,
			@NonNull OAuth2AccessToken accessToken
			) {

		 return
	                databaseAdapter
	                        .getConsumerSaleRegDraftWithAccountId(
	                        		accountId
	                        );
	}

}
