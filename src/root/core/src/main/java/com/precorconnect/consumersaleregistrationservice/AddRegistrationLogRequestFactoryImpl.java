package com.precorconnect.consumersaleregistrationservice;

import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;

@Singleton
public class AddRegistrationLogRequestFactoryImpl 
				implements AddRegistrationLogRequestFactory {

	@Override
	public AddRegistrationLog construct(
			@NonNull ConsumerSaleRegDraftView consumerSaleRegDraftView,
			@NonNull SubmittedByName submittedByName,
			@Nullable String partnerRepId,
			@Nullable String partnerRepFirstName,
			@Nullable String partnerRepLastName,
			@Nullable String partnerRepEmailAddress
			) {

		Long partnerSaleRegistrationId = 
				consumerSaleRegDraftView
							.getId().getValue();
			
		String accountId = 
				consumerSaleRegDraftView.getPartnerAccountId().getValue();
						
		String accountName = 
				consumerSaleRegDraftView.getFirstName().getValue()
				+" "+ consumerSaleRegDraftView.getLastName().getValue();
		
		String installDate = null;
		
		String sellDate =
				 new SimpleDateFormat("MM/dd/yyyy").format(
						 Date.from(consumerSaleRegDraftView
						 .getSellDate())
				 	);
		
		String submittedDate =
				 new SimpleDateFormat("MM/dd/yyyy").format(
						 Date.from(consumerSaleRegDraftView
						 .getSaleCreatedAtTimestamp())
				 	);
		
		String extendedWarrantyStatus =
						"Not Applicable";
		
		
		return 
				new AddRegistrationLog(
						partnerSaleRegistrationId, 
						accountId, 
						accountName, 
						sellDate,
						installDate,
						submittedDate,
						extendedWarrantyStatus,
						partnerRepFirstName,
						partnerRepLastName,
						partnerRepEmailAddress,
						partnerRepId,
						submittedByName.getValue()
						);

	}
}
