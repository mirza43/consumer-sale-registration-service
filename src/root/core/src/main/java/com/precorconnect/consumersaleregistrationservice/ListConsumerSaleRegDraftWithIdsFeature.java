package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

interface ListConsumerSaleRegDraftWithIdsFeature {

	Collection<ConsumerPartnerSaleRegDraftView> execute(
			@NonNull Collection<ConsumerSaleRegDraftId> consumerSaleRegDraftIds,
			@NonNull OAuth2AccessToken accessToken
    );
	
}
