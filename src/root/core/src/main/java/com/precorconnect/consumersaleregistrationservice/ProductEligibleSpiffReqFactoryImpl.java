package com.precorconnect.consumersaleregistrationservice;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleCompositeLineItemWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleSimpleLineItemWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

@Singleton
public class ProductEligibleSpiffReqFactoryImpl implements ProductEligibleSpiffReqFactory {
	
	@Override
	public ProductSaleRegistrationRuleEngineWebDto construct(
			@NonNull ProductSaleRegistrationRuleEngineRequestDto productSaleRegistrationRuleEngineRequestDto
			) {
		
		ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto
		= new ProductSaleRegistrationRuleEngineWebDto();

			SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			
			String installDate = format.format(
										Date.from(productSaleRegistrationRuleEngineRequestDto
												.getInstallDate().isPresent()
												?productSaleRegistrationRuleEngineRequestDto
												 .getInstallDate().get().getValue()
												:null
												)
									);
			
			String sellDate = format.format(
									Date.from(
											productSaleRegistrationRuleEngineRequestDto
											.getSellDate().getValue()
											)
									);
			
			String submittedDate = format.format(
										Date.from(
												new Date().toInstant()
											)
										);

				productSaleRegistrationRuleEngineWebDto
					.setSellDate(sellDate);
				
				productSaleRegistrationRuleEngineWebDto
					.setInstallDate(installDate);
				
				productSaleRegistrationRuleEngineWebDto
					.setSubmittedDate(submittedDate);
				
				productSaleRegistrationRuleEngineWebDto
					.setFacilityBrand(
							productSaleRegistrationRuleEngineRequestDto
								.getBrand().isPresent()
								?productSaleRegistrationRuleEngineRequestDto
										.getBrand().get().getValue()
										:null
							);
				
				productSaleRegistrationRuleEngineWebDto
					.setRegistrationId(
							productSaleRegistrationRuleEngineRequestDto
							.getPartnerSaleRegId().isPresent()
							?productSaleRegistrationRuleEngineRequestDto
									.getPartnerSaleRegId().get().getValue():null
							);
				
				 List<PartnerSaleSimpleLineItemWebDto> lineItems = 
						 new ArrayList<PartnerSaleSimpleLineItemWebDto>();
				    
				 List<PartnerSaleCompositeLineItemWebDto> compositeLineItems = 
						 new ArrayList<PartnerSaleCompositeLineItemWebDto>();
				  
				 List<PartnerSaleLineItemDto> simpleLineItems = 
						 productSaleRegistrationRuleEngineRequestDto
						 	.getSimpleLineItems();
  
  			for(PartnerSaleLineItemDto partnerSaleLineItemDto:simpleLineItems){
  				
  				PartnerSaleSimpleLineItemWebDto partnerSaleLineItemWebDto = 
  						new PartnerSaleSimpleLineItemWebDto();
  				
  				partnerSaleLineItemWebDto
  					.setSerialNumber(
  							partnerSaleLineItemDto
  								.getSerialNumber()
  								.getValue()
  								);
  				
  				lineItems.add(partnerSaleLineItemWebDto);
  			
  			}
  			
  			productSaleRegistrationRuleEngineWebDto.setSimpleLineItems(lineItems);	  
  			
  			List<PartnerCompositeSaleLineItemDto> listCompositeLineItems = 
  					productSaleRegistrationRuleEngineRequestDto
  						.getCompositeLineItems();
  		
  		for(PartnerCompositeSaleLineItemDto partnerCompositeSaleLineItemDto:listCompositeLineItems){
  			
  			PartnerSaleCompositeLineItemWebDto component = 
  					new PartnerSaleCompositeLineItemWebDto();
  			
  			Collection<PartnerSaleSimpleLineItemWebDto> compositeLineItemComponents = 
  					new ArrayList<PartnerSaleSimpleLineItemWebDto>();
  			
  			Collection<PartnerSaleLineItemDto> components = partnerCompositeSaleLineItemDto.getComponents();
  			
  			for(PartnerSaleLineItemDto partnerSaleLineItemDto:components){
  				
  				PartnerSaleSimpleLineItemWebDto partnerSaleLineItemWebDtoComponent = 
  						new PartnerSaleSimpleLineItemWebDto();
  				
  				partnerSaleLineItemWebDtoComponent
  						.setSerialNumber(
  								partnerSaleLineItemDto
  									.getSerialNumber()
  									.getValue()
  									);
  				
  				compositeLineItemComponents.add(partnerSaleLineItemWebDtoComponent);
  			
  			}
  			
  			component.setComponents(compositeLineItemComponents);
  			
			compositeLineItems.add(component);
  			
  		}
			
			productSaleRegistrationRuleEngineWebDto.
				setCompositeLineItems(compositeLineItems);
			
  			
		return productSaleRegistrationRuleEngineWebDto;
	}

}
