package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class GetConsumerSaleRegDraftWithIdFeatureImpl
        implements GetConsumerSaleRegDraftWithIdFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    /*
    constructors
     */
    @Inject
    public GetConsumerSaleRegDraftWithIdFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public ConsumerPartnerSaleRegDraftView execute(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
            @NonNull OAuth2AccessToken accessToken
    ) {

        return
                databaseAdapter
                        .getConsumerSaleRegDraftWithId(
                        		consumerSaleRegDraftId
                        );

    }
}
