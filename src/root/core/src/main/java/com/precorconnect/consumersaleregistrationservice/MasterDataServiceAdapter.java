package com.precorconnect.consumersaleregistrationservice;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineResponseWebView;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

public interface MasterDataServiceAdapter {

	ProductSaleRegistrationRuleEngineResponseWebView getSpiffAmountForPartnerCommercialSaleRegistration(
			@NonNull ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;
	
	Collection<SpiffSerialExclusionDto> listSpiffSerialExclusion(
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException;
}
