package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.AmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.LineIdImpl;
import com.precorconnect.claimspiffservice.objectmodel.RegistrationIDImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffAmountImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffDetailsDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffDetailsDtoImpl;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.objectmodel.SpiffTypeImpl;
import com.precorconnect.registrationlogservice.webapi.UpdateRegistrationLog;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleCompositeLineItemResWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleCompositeLineItemWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleLineItemResWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleSimpleLineItemResWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.PartnerSaleSimpleLineItemWebDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineResponseWebView;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

@Singleton
class SubmitConsumerSaleRegDraftFeatureImpl
        implements SubmitConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;

    private final ClaimSpiffServiceAdapter spiffEntitlementServiceAdapter;

    private final MasterDataServiceAdapter masterDataServiceAdapter;
    
    private final RegistrationLogServiceAdapter registrationLogServiceAdapter;

    private final CreateSpiffEntitlementsRequestFactory createSpiffEntitlementsRequestFactory;

    private final GetSpiffAmountRequestFactory getSpiffAmountRequestFactory;
    
    private final AddRegistrationLogRequestFactory addRegistrationLogRequestFactory;

    /*
    constructors
     */
    @Inject
    public SubmitConsumerSaleRegDraftFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter,
            @NonNull ClaimSpiffServiceAdapter spiffEntitlementServiceAdapter,
            @NonNull MasterDataServiceAdapter masterDataServiceAdapter,
            @NonNull RegistrationLogServiceAdapter registrationLogServiceAdapter,
            @NonNull CreateSpiffEntitlementsRequestFactory createSpiffEntitlementsRequestFactory,
            @NonNull GetSpiffAmountRequestFactory getSpiffAmountRequestFactory,
            @NonNull AddRegistrationLogRequestFactory addRegistrationLogRequestFactory
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.spiffEntitlementServiceAdapter =
                guardThat(
                        "spiffEntitlementServiceAdapter",
                        spiffEntitlementServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.masterDataServiceAdapter =
                guardThat(
                        "masterDataServiceAdapter",
                        masterDataServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	
    	this.registrationLogServiceAdapter =
                guardThat(
                        "registrationLogServiceAdapter",
                        registrationLogServiceAdapter
                )
                        .isNotNull()
                        .thenGetValue();

    	this.createSpiffEntitlementsRequestFactory =
                guardThat(
                        "createSpiffEntitlementsRequestFactory",
                        createSpiffEntitlementsRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.getSpiffAmountRequestFactory =
                guardThat(
                        "getSpiffAmountRequestFactory",
                        getSpiffAmountRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.addRegistrationLogRequestFactory =
                guardThat(
                        "addRegistrationLogRequestFactory",
                        addRegistrationLogRequestFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public ConsumerPartnerSaleRegDraftView execute(
            @NonNull SubmitConsumerSaleRegDraftRequestDto submitConsumerSaleRegDraftRequestDto,
            @NonNull OAuth2AccessToken accessToken
    		) throws AuthenticationException, AuthorizationException {
    	
    	boolean flag=false;
    	
    	ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto = null;
    	
    	ConsumerPartnerSaleRegDraftView consumerPartnerSaleRegDraftView
			    	= databaseAdapter
			        			.submitConsumerSaleRegDraft(
			        					submitConsumerSaleRegDraftRequestDto.getId(),
			        					accessToken
			        					);
    	
    	if(consumerPartnerSaleRegDraftView!=null) {
    		
    		Collection<SpiffSerialExclusionDto> spiffSerialExclusionList = 
    				masterDataServiceAdapter
						.listSpiffSerialExclusion(accessToken);
    		
    		Collection<SerialNumber> serialNumberExclusionList=new ArrayList<>();
          	
       	   for(SpiffSerialExclusionDto spiffSerialExclusionDto:spiffSerialExclusionList){
       		serialNumberExclusionList.add(spiffSerialExclusionDto.getSerialNumber());
       	   }
     	  
     	  registrationLogServiceAdapter
    				.addRegistrationLog(
    						addRegistrationLogRequestFactory
    						.construct(
    								consumerPartnerSaleRegDraftView,
    								submitConsumerSaleRegDraftRequestDto.getSubmittedByName(),
    								submitConsumerSaleRegDraftRequestDto
									.getPartnerRep().isPresent()?
											submitConsumerSaleRegDraftRequestDto
											.getPartnerRep()
											.get()
											.getValue():null,
									submitConsumerSaleRegDraftRequestDto
											.getFirstName().isPresent()?
											submitConsumerSaleRegDraftRequestDto
											.getFirstName()
											.get()
											.getValue():null,
									submitConsumerSaleRegDraftRequestDto
											.getLastName().isPresent()?
											submitConsumerSaleRegDraftRequestDto
											.getLastName()
											.get()
											.getValue():null,
									submitConsumerSaleRegDraftRequestDto
											.getEmailAddress().isPresent()?
											submitConsumerSaleRegDraftRequestDto
											.getEmailAddress()
											.get()
											.getValue():null
    						),
    						accessToken
    					);
    	    	
    	 productSaleRegistrationRuleEngineWebDto = 
    			getSpiffAmountRequestFactory
					.construct(consumerPartnerSaleRegDraftView,serialNumberExclusionList);
    	
    	Collection<PartnerSaleSimpleLineItemWebDto> simpleLineItemWebDtoList=new ArrayList<>();
    	
    	Iterable<PartnerSaleSimpleLineItemWebDto> simpleLineItemWebDtoIterator =productSaleRegistrationRuleEngineWebDto.getSimpleLineItems()
    																.isPresent()?
    																productSaleRegistrationRuleEngineWebDto.getSimpleLineItems().get():null;
    	
        for(PartnerSaleSimpleLineItemWebDto partnerSaleSimpleLineItemWebDto:simpleLineItemWebDtoIterator){
        	simpleLineItemWebDtoList.add(partnerSaleSimpleLineItemWebDto);
        }
        
        Collection<PartnerSaleCompositeLineItemWebDto> compositeLineItemsWebDtoList=new ArrayList<>();
        
        Iterable<PartnerSaleCompositeLineItemWebDto> compositeLineItemWebDtoIterator=
																	productSaleRegistrationRuleEngineWebDto.getCompositeLineItems()
																	.isPresent()?
																	productSaleRegistrationRuleEngineWebDto.getCompositeLineItems().get():null;	
																	
		for(PartnerSaleCompositeLineItemWebDto partnerSaleCompositeLineItemWebDto:compositeLineItemWebDtoIterator){
			compositeLineItemsWebDtoList.add(partnerSaleCompositeLineItemWebDto);
		}
		
		if(simpleLineItemWebDtoList.size()==0 && compositeLineItemsWebDtoList.size()==0){
			
			flag=true;
			Collection<SerialNumber> emptySerialNumberExclusionList=new ArrayList<>();
			
			productSaleRegistrationRuleEngineWebDto = 
	    			getSpiffAmountRequestFactory
						.construct(consumerPartnerSaleRegDraftView,emptySerialNumberExclusionList);
			
		}
    																
    	
		ProductSaleRegistrationRuleEngineResponseWebView productSaleRegistrationRuleEngineResponseWebView = 
				masterDataServiceAdapter 
					.getSpiffAmountForPartnerCommercialSaleRegistration(
							productSaleRegistrationRuleEngineWebDto,
				    		accessToken
    		    			);
    	
			if(null != productSaleRegistrationRuleEngineResponseWebView.getAmount()
				&& productSaleRegistrationRuleEngineResponseWebView.getAmount() > 0){
				
				Double amount=0.0;
	
		    	List<SpiffEntitlementDto> entitlements = new ArrayList<SpiffEntitlementDto>();
		    	
		    	if(!flag){
		    		amount=productSaleRegistrationRuleEngineResponseWebView.getAmount();
		    	}
	
		    	SpiffEntitlementDto dto =
						createSpiffEntitlementsRequestFactory
							.construct(
									consumerPartnerSaleRegDraftView,
										new SpiffAmountImpl(
												amount
										    	)
									);
		    	
		    	entitlements.add(dto);
	
		    	spiffEntitlementServiceAdapter
		    			.createSpiffEntitlements(
		    					entitlements,
						        accessToken
		    					);
		    	
		    	Collection<SpiffDetailsDto> spiffDetailsList= new ArrayList<>();
		    	
		    	Collection<PartnerSaleSimpleLineItemResWebDto> simpleLineItems= productSaleRegistrationRuleEngineResponseWebView
		    																										.getSimpleLineItems();
		    	
		    	Collection<ConsumerSaleRegDraftSaleLineItem>	saleLineItems= consumerPartnerSaleRegDraftView.getSaleLineItems();
		    	
		    	if(simpleLineItems.size()>0){
		    	
		    	for(PartnerSaleSimpleLineItemResWebDto partnerSaleSimpleLineItemResWebDto:simpleLineItems){
		    		
		    		for(ConsumerSaleRegDraftSaleLineItem lineItem:saleLineItems){
		    			
		    			if(lineItem instanceof com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem){
		    				
		    			String serialNumber=((com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem) lineItem)
																								.getSerialNumber()
																								.getValue();
							
		    			SerialNumber assetSerialNumber = 
						     		    			new SerialNumberImpl(
						     		    					serialNumber
						     		    						);
		    			
		    			if(!serialNumberExclusionList.contains(assetSerialNumber)){
		    				
		    			if(	partnerSaleSimpleLineItemResWebDto.getSerialNumber().equalsIgnoreCase(serialNumber.substring(0, 4))){
		    				
		    				SpiffDetailsDto spiffDetailsDto=
		    						new SpiffDetailsDtoImpl(
		    								new RegistrationIDImpl(consumerPartnerSaleRegDraftView.getId().getValue()),
		    								new LineIdImpl(lineItem.getId().getValue()), 
		    								new AmountImpl(partnerSaleSimpleLineItemResWebDto.getAmount()), 
		    								new SpiffTypeImpl(partnerSaleSimpleLineItemResWebDto.getDescrition())
		    							);
		    				
		    				spiffDetailsList.add(spiffDetailsDto);
		    					
		    				}
		    			}
		    			
		    			}
		    			
		    		}
		    		
		    	}
		    	
			}
		    	
		    	Collection<PartnerSaleCompositeLineItemResWebDto> compositeLineItems = productSaleRegistrationRuleEngineResponseWebView
		    																											.getCompositeLineItems();
		    	
		    	if(compositeLineItems.size()>0){
		    	
		    	for(PartnerSaleCompositeLineItemResWebDto partnerSaleCompositeLineItemResWebDto:compositeLineItems){
		    		
		    		StringBuilder ruleEngineResComponentsSerialNumber=new StringBuilder();
		    		
		    		StringBuilder description=new StringBuilder();
		    		
		    		Collection<String> ruleEngineSerialNumbersList=new ArrayList<>();
		    		
		    		for(PartnerSaleLineItemResWebDto partnerSaleLineItemResWebDto:partnerSaleCompositeLineItemResWebDto.getComponents()){
		    			
		    			ruleEngineSerialNumbersList.add(partnerSaleLineItemResWebDto.getSerialNumber());
		    			description.append(partnerSaleLineItemResWebDto.getDescrition()+"/");
		    		
		    		}
		    		
		    		Collections.sort((List<String>)ruleEngineSerialNumbersList, new SerialNumberComparator());
		    		
		    		for(String serialNumber:ruleEngineSerialNumbersList){
		    			ruleEngineResComponentsSerialNumber.append(serialNumber);
		    		}
		    		
			    		for(ConsumerSaleRegDraftSaleLineItem lineItem:saleLineItems){
			    			
			    		if (lineItem instanceof com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem){
			    			
			    			StringBuilder partnerSaleComponentsSerialNumber=new StringBuilder();
			    			
			    			Collection<String> partnerSaleSerialNumbersList=new ArrayList<>();
			    			
			    			for(ConsumerSaleLineItemComponent consumerSaleLineItemComponent:
			    				((com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem) lineItem).getComponents()){
			    				
			    				SerialNumber serialNumber = 
		     		    				new SerialNumberImpl(
		     		    						consumerSaleLineItemComponent
		     		    							.getSerialNumber()
		     		    							.getValue()
		     		    							);
			    				if(!serialNumberExclusionList.contains(serialNumber)){
			    				partnerSaleSerialNumbersList.add(consumerSaleLineItemComponent.getSerialNumber().getValue().substring(0, 4));
			    				}
			    			
			    			}
			    			
			    			Collections.sort((List<String>)partnerSaleSerialNumbersList, new SerialNumberComparator());
			    		    
			    		    for(String serialNumber:partnerSaleSerialNumbersList){
			    				partnerSaleComponentsSerialNumber.append(serialNumber);
				    		}
			    		    
			    			if(ruleEngineResComponentsSerialNumber.toString().equalsIgnoreCase(partnerSaleComponentsSerialNumber.toString())){
			    				
			    				SpiffDetailsDto spiffDetailsDto=
			    						new SpiffDetailsDtoImpl(
			    								new RegistrationIDImpl(consumerPartnerSaleRegDraftView.getId().getValue()),
			    								new LineIdImpl(lineItem.getId().getValue()), 
			    								new AmountImpl(partnerSaleCompositeLineItemResWebDto.getAmount()), 
			    								new SpiffTypeImpl(description.toString())
			    							);
			    				
			    				spiffDetailsList.add(spiffDetailsDto);
			    				
			    			}
			    			
			    			}
			    		}
		    		
		    	}
		    	
			}
		    	
		    	if(spiffDetailsList.size()>0){
		    	spiffEntitlementServiceAdapter
		    							.addSpiffDetails(
		    									spiffDetailsList,
		    									accessToken
		    							);
		    	}
		    	
		    	if(flag){
		    		
		    		List<UpdateRegistrationLog> updateRegLogList = 
							new ArrayList<UpdateRegistrationLog>();
					
					updateRegLogList
						.add(
								new UpdateRegistrationLog(
										consumerPartnerSaleRegDraftView
										.getId()
										.getValue(),
										"Not Applicable"
									)
							);
					
					registrationLogServiceAdapter
						.updateRegistrationLog(
								updateRegLogList, 
								accessToken
								);
		    	}
		    	
	   }else{
			    		
			    		List<UpdateRegistrationLog> updateRegLogList = 
								new ArrayList<UpdateRegistrationLog>();
						
						updateRegLogList
							.add(
									new UpdateRegistrationLog(
											consumerPartnerSaleRegDraftView
											.getId()
											.getValue(),
											"Not Applicable"
										)
								);
						
						registrationLogServiceAdapter
							.updateRegistrationLog(
									updateRegLogList, 
									accessToken
									);
	   }
						
	}
		    	
    	
    	return consumerPartnerSaleRegDraftView;
    }
}
