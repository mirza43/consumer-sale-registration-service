package com.precorconnect.consumersaleregistrationservice;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

import com.precorconnect.registrationlogservice.webapi.AddRegistrationLog;

public interface AddRegistrationLogRequestFactory {

	AddRegistrationLog construct(
			@NonNull ConsumerSaleRegDraftView consumerSaleRegDraftView,
			@NonNull SubmittedByName submittedByName,
			@Nullable String partnerRepId,
			@Nullable String firstName,
			@Nullable String lastName,
			@Nullable String emailAddress
			);
}
