package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

interface AddSaleLineItemToConsumerSaleRegDraftFeature {

    ConsumerSaleRegDraftSaleLineItemId execute(
            @NonNull AddSaleLineItemToConsumerSaleRegDraftReq request,
            @NonNull OAuth2AccessToken accessToken
    );

}
