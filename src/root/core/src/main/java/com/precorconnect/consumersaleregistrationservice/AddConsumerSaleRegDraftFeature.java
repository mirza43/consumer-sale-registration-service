package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.OAuth2AccessToken;
import org.checkerframework.checker.nullness.qual.NonNull;

interface AddConsumerSaleRegDraftFeature {

    ConsumerSaleRegDraftId execute(
            @NonNull AddConsumerSaleRegDraftReq request,
            @NonNull OAuth2AccessToken accessToken
    );

}
