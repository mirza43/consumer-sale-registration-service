package com.precorconnect.consumersaleregistrationservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;

@Singleton
public class GetConsumerSaleDraftExcludeSerialNumberFeatureImpl
		implements GetConsumerSaleDraftExcludeSerialNumberFeature {
	
	 /*
    fields
     */
    private final DatabaseAdapter databaseAdapter;
    
    private final MasterDataServiceAdapter masterDataServiceAdapter;

    /*
    constructors
     */
    @Inject
    public GetConsumerSaleDraftExcludeSerialNumberFeatureImpl(
            @NonNull DatabaseAdapter databaseAdapter,
            @NonNull MasterDataServiceAdapter masterDataServiceAdapter
    ) {

    	this.databaseAdapter =
                guardThat(
                        "databaseAdapter",
                         databaseAdapter
                )
                        .isNotNull()
                        .thenGetValue();
    	
    	this.masterDataServiceAdapter=
    			 guardThat(
                         "masterDataServiceAdapter",
                         masterDataServiceAdapter
                 )
                         .isNotNull()
                         .thenGetValue();
    }
	
	@Override
	public ConsumerPartnerSaleRegDraftView execute(@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull OAuth2AccessToken accessToken) throws AuthenticationException, AuthorizationException {
		
		
		Collection<SpiffSerialExclusionDto> spiffSerialExclusionList=masterDataServiceAdapter.listSpiffSerialExclusion(accessToken);
		
		Collection<SerialNumber> serialNumberExclusionList=new ArrayList<>();
		
		for(SpiffSerialExclusionDto spiffSerialExclusionDto:spiffSerialExclusionList){
			serialNumberExclusionList.add(spiffSerialExclusionDto.getSerialNumber());
		}
		
		return 
				databaseAdapter
								.getConsumerSaleRegDraftExcludeSerialNumber(
															consumerSaleRegDraftId,
															serialNumberExclusionList
														);
	}

}
