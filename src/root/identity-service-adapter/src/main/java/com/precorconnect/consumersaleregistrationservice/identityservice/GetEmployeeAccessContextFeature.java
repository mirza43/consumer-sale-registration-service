package com.precorconnect.consumersaleregistrationservice.identityservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.EmployeeAccessContext;
import com.precorconnect.OAuth2AccessToken;

public interface GetEmployeeAccessContextFeature {

	EmployeeAccessContext execute(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

}
