package com.precorconnect.consumersaleregistrationservice.identityservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.EmployeeAccessContext;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.identityservice.sdk.IdentityServiceSdk;

@Singleton
public class GetEmployeeAccessContextFeatureImpl implements GetEmployeeAccessContextFeature {

	/*
    fields
     */
    private final IdentityServiceSdk identityServiceSdk;

    /*
    constructors
     */
    @Inject
    public GetEmployeeAccessContextFeatureImpl(
            @NonNull final IdentityServiceSdk identityServiceSdk
    ) {

    	this.identityServiceSdk =
                guardThat(
                        "identityServiceSdk",
                        identityServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public EmployeeAccessContext execute(@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException {

		return identityServiceSdk
				.getEmployeeAccessContext(
								accessToken
								);
	}

}
