package com.precorconnect.consumersaleregistrationservice.identityservice;

import java.net.URL;

public interface IdentityServiceAdapterConfig {

    URL getPrecorConnectApiBaseUrl();

}
