package com.precorconnect.consumersaleregistrationservice.identityservice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.*;
import com.precorconnect.consumersaleregistrationservice.IdentityServiceAdapter;

import org.checkerframework.checker.nullness.qual.NonNull;

import javax.inject.Singleton;

@Singleton
public class IdentityServiceAdapterImpl implements
        IdentityServiceAdapter {

    /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public IdentityServiceAdapterImpl(
            @NonNull IdentityServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }

    @Override
    public AccessContext getAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return
                injector
                        .getInstance(GetAccessContextFeature.class)
                        .execute(accessToken);

    }

    @Override
    public AppAccessContext getAppAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return
                injector
                        .getInstance(GetAppAccessContextFeature.class)
                        .execute(accessToken);

    }

    @Override
    public PartnerRepAccessContext getPartnerRepAccessContext(
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return
                injector
                        .getInstance(GetPartnerRepAccessContextFeature.class)
                        .execute(accessToken);

    }

	@Override
	public EmployeeAccessContext getEmployeeAccessContext(
			@NonNull OAuth2AccessToken accessToken
			)throws AuthenticationException {
	
	return	
			injector
			        .getInstance(GetEmployeeAccessContextFeature.class)
			        .execute(accessToken);

	}
}
