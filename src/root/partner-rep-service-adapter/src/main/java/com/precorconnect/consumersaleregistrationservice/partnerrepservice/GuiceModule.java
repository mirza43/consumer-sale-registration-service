package com.precorconnect.consumersaleregistrationservice.partnerrepservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdk;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdkConfig;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdkConfigImpl;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdkImpl;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final PartnerRepServiceAdapterConfig config;

    /*
    constructors
     */
    public GuiceModule(
            @NonNull PartnerRepServiceAdapterConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();
    }

    @Override
    protected void configure() {

        bind(PartnerRepServiceSdkConfig.class)
                .toInstance(
                        new PartnerRepServiceSdkConfigImpl(
                                config.getPrecorConnectApiBaseUrl()
                        )
                );

        bind(PartnerRepServiceSdk.class)
                .to(PartnerRepServiceSdkImpl.class)
                .in(Singleton.class);

        bindFeatures();
        
        bindFactories();

    }

    private void bindFeatures() {
    	
    	 bind(GetPartnerRepsWithIdFeature.class)
         		.to(GetPartnerRepsWithIdFeatureImpl.class);

    }
    
    private void bindFactories(){
  	  
  }

}
