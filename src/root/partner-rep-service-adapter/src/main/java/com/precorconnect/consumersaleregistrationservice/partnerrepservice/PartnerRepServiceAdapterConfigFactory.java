package com.precorconnect.consumersaleregistrationservice.partnerrepservice;

public interface PartnerRepServiceAdapterConfigFactory {

	PartnerRepServiceAdapterConfig construct();

}
