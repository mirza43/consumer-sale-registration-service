package com.precorconnect.consumersaleregistrationservice.partnerrepservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.partnerrepservice.PartnerRepView;
import com.precorconnect.partnerrepservice.sdk.PartnerRepServiceSdk;

@Singleton
public class GetPartnerRepsWithIdFeatureImpl implements GetPartnerRepsWithIdFeature {
	
	/*
    fields
     */
    private final PartnerRepServiceSdk partnerRepServiceSdk;

    /*
    constructors
     */
    @Inject
    public GetPartnerRepsWithIdFeatureImpl(
            @NonNull PartnerRepServiceSdk partnerRepServiceSdk
    ) {

    	this.partnerRepServiceSdk =
                guardThat(
                        "partnerRepServiceSdk",
                        partnerRepServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public PartnerRepView execute(@NonNull UserId id,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
		
		return 
					partnerRepServiceSdk.getPartnerRepWithId(id, accessToken);
	}

}
