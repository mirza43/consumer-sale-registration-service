package com.precorconnect.consumersaleregistrationservice.partnerrepservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.UserId;
import com.precorconnect.consumersaleregistrationservice.PartnerRepServiceAdapter;
import com.precorconnect.partnerrepservice.PartnerRepView;

public class PartnerRepServiceAdapterImpl implements PartnerRepServiceAdapter {
	
	 /*
    fields
     */
    private final Injector injector;

    /*
    constructors
     */
    public PartnerRepServiceAdapterImpl(
            @NonNull PartnerRepServiceAdapterConfig config
    ) {

        GuiceModule guiceModule =
                new GuiceModule(config);

        injector =
                Guice.createInjector(guiceModule);
    }

	@Override
	public PartnerRepView getPartnerRepWithId(@NonNull UserId id,
			@NonNull OAuth2AccessToken accessToken)
			throws AuthenticationException, AuthorizationException {
		
		return
	                injector
	                        .getInstance(GetPartnerRepsWithIdFeature.class)
	                        .execute(id, accessToken);

	    }
	

}
