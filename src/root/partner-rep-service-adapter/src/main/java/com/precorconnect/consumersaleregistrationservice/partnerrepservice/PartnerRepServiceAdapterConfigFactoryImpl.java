package com.precorconnect.consumersaleregistrationservice.partnerrepservice;

import java.net.MalformedURLException;
import java.net.URL;

public class PartnerRepServiceAdapterConfigFactoryImpl
        implements PartnerRepServiceAdapterConfigFactory {

    @Override
    public PartnerRepServiceAdapterConfig construct() {

        return new PartnerRepServiceAdapterConfigImpl(
                constructPrecorConnectApiBaseUrl()
        );

    }

    private URL constructPrecorConnectApiBaseUrl() {

        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

        try {

            return new URL(precorConnectApiBaseUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }
}
