package com.precorconnect.consumersaleregistrationservice.partnerrepservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.net.URL;

import static com.precorconnect.guardclauses.Guards.guardThat;

public class PartnerRepServiceAdapterConfigImpl
        implements PartnerRepServiceAdapterConfig {

    /*
    fields
     */
    private final URL precorConnectApiBaseUrl;

    /*
    constructors
     */
    public PartnerRepServiceAdapterConfigImpl(
            @NonNull URL precorConnectApiBaseUrl
    ) {

    	this.precorConnectApiBaseUrl =
                guardThat(
                        "precorConnectApiBaseUrl",
                         precorConnectApiBaseUrl
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public URL getPrecorConnectApiBaseUrl() {
        return precorConnectApiBaseUrl;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PartnerRepServiceAdapterConfigImpl that = (PartnerRepServiceAdapterConfigImpl) o;

        return precorConnectApiBaseUrl.equals(that.precorConnectApiBaseUrl);

    }

    @Override
    public int hashCode() {
        return precorConnectApiBaseUrl.hashCode();
    }
}
