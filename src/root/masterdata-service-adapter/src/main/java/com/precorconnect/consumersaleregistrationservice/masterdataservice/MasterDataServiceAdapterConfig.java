package com.precorconnect.consumersaleregistrationservice.masterdataservice;

import java.net.URL;

public interface MasterDataServiceAdapterConfig {

    URL getPrecorConnectApiBaseUrl();

}
