package com.precorconnect.consumersaleregistrationservice.masterdataservice;


import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.SpiffSerialExclusionDto;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.SpiffSerialExclusionWebView;

public interface SpiffSerialExclusionResponseFactory {
	
	SpiffSerialExclusionDto construct(
			@NonNull SpiffSerialExclusionWebView spiffSerialExclusion
			);

}
