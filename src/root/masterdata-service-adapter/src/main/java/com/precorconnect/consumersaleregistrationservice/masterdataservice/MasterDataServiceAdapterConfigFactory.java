package com.precorconnect.consumersaleregistrationservice.masterdataservice;

public interface MasterDataServiceAdapterConfigFactory {

	MasterDataServiceAdapterConfig construct();

}
