package com.precorconnect.consumersaleregistrationservice.masterdataservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffmasterdataservice.sdk.SpiffRuleEngineServiceSdk;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineResponseWebView;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

@Singleton
class GetSpiffAmountForPartnerCommercialSaleRegistrationFeatureImpl
        implements GetSpiffAmountForPartnerCommercialSaleRegistrationFeature {

    /*
    fields
     */
    private final SpiffRuleEngineServiceSdk spiffRuleEngineServiceSdk;

    /*
    constructors
     */
    @Inject
    public GetSpiffAmountForPartnerCommercialSaleRegistrationFeatureImpl(
            @NonNull SpiffRuleEngineServiceSdk spiffRuleEngineServiceSdk
    ) {

    	this.spiffRuleEngineServiceSdk =
                guardThat(
                        "spiffRuleEngineServiceSdk",
                        spiffRuleEngineServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
	public ProductSaleRegistrationRuleEngineResponseWebView execute(
    		@NonNull ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException {

        return
        		spiffRuleEngineServiceSdk
                			.executeSpiffRuleEngine(
                					productSaleRegistrationRuleEngineWebDto,
                					accessToken
                				);


    }
}
