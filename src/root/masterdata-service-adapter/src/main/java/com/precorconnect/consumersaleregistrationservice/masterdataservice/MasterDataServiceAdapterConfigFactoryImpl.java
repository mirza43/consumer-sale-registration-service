package com.precorconnect.consumersaleregistrationservice.masterdataservice;

import java.net.MalformedURLException;
import java.net.URL;

public class MasterDataServiceAdapterConfigFactoryImpl
        implements MasterDataServiceAdapterConfigFactory {

    @Override
    public MasterDataServiceAdapterConfig construct() {

        return new MasterDataServiceAdapterConfigImpl(
                constructPrecorConnectApiBaseUrl()
        );

    }

    private URL constructPrecorConnectApiBaseUrl() {

        String precorConnectApiBaseUrl = System.getenv("PRECOR_CONNECT_API_BASE_URL");

        try {

            return new URL(precorConnectApiBaseUrl);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }

    }
}
