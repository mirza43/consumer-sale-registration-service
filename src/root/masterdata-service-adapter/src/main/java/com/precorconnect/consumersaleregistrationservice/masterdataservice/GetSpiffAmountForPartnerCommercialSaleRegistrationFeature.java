package com.precorconnect.consumersaleregistrationservice.masterdataservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineResponseWebView;
import com.precorconnect.spiffmasterdataservice.webapiobjectmodel.ProductSaleRegistrationRuleEngineWebDto;

interface GetSpiffAmountForPartnerCommercialSaleRegistrationFeature {

	ProductSaleRegistrationRuleEngineResponseWebView execute(
			@NonNull ProductSaleRegistrationRuleEngineWebDto productSaleRegistrationRuleEngineWebDto,
            @NonNull OAuth2AccessToken accessToken
    ) throws AuthenticationException;

}
