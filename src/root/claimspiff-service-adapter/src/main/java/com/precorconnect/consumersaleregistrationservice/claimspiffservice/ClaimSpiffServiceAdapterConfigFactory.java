package com.precorconnect.consumersaleregistrationservice.claimspiffservice;

public interface ClaimSpiffServiceAdapterConfigFactory {

	ClaimSpiffServiceAdapterConfig construct();

}
