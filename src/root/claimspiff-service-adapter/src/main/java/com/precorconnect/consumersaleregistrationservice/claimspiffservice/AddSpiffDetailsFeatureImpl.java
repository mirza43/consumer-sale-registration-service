package com.precorconnect.consumersaleregistrationservice.claimspiffservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.SpiffDetailsDto;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdk;

@Singleton
public class AddSpiffDetailsFeatureImpl implements AddSpiffDetailsFeature {
	
	/*
    fields
     */
    private final ClaimSpiffServiceSdk claimSpiffServiceSdk;

    /*
    constructors
     */
    @Inject
    public AddSpiffDetailsFeatureImpl(
            @NonNull ClaimSpiffServiceSdk claimSpiffServiceSdk
    ) {

    	this.claimSpiffServiceSdk =
                guardThat(
                        "claimSpiffServiceSdk",
                        claimSpiffServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public void execute(
			@NonNull Collection<SpiffDetailsDto> spiffDetailsList,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException{

         claimSpiffServiceSdk
                .addSpiffDetails(
                		spiffDetailsList,
                		accessToken
                	);
    }
}
