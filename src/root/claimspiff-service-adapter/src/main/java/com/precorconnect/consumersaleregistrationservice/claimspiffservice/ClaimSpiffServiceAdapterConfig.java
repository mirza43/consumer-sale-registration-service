package com.precorconnect.consumersaleregistrationservice.claimspiffservice;

import java.net.URL;

public interface ClaimSpiffServiceAdapterConfig {

    URL getPrecorConnectApiBaseUrl();

}
