package com.precorconnect.consumersaleregistrationservice.claimspiffservice;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.SpiffEntitlementDto;
import com.precorconnect.claimspiffservice.sdk.ClaimSpiffServiceSdk;

@Singleton
class CreateSpiffEntitlementFeatureImpl
        implements CreateSpiffEntitlementFeature {

    /*
    fields
     */
    private final ClaimSpiffServiceSdk claimSpiffServiceSdk;

    /*
    constructors
     */
    @Inject
    public CreateSpiffEntitlementFeatureImpl(
            @NonNull ClaimSpiffServiceSdk claimSpiffServiceSdk
    ) {

    	this.claimSpiffServiceSdk =
                guardThat(
                        "claimSpiffServiceSdk",
                        claimSpiffServiceSdk
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public Collection<Long> createSpiffEntitlements(
			@NonNull List<SpiffEntitlementDto> spiffEntitlements,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException {

        return claimSpiffServiceSdk
                .createSpiffEntitlements(
                		spiffEntitlements,
                        accessToken
                );

    }
}
