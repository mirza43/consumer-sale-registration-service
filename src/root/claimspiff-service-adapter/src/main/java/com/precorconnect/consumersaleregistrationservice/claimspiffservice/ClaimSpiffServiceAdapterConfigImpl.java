package com.precorconnect.consumersaleregistrationservice.claimspiffservice;

import org.checkerframework.checker.nullness.qual.NonNull;

import java.net.URL;

import static com.precorconnect.guardclauses.Guards.guardThat;

public class ClaimSpiffServiceAdapterConfigImpl
        implements ClaimSpiffServiceAdapterConfig {

    /*
    fields
     */
    private final URL precorConnectApiBaseUrl;

    /*
    constructors
     */
    public ClaimSpiffServiceAdapterConfigImpl(
            @NonNull URL precorConnectApiBaseUrl
    ) {

    	this.precorConnectApiBaseUrl =
                guardThat(
                        "precorConnectApiBaseUrl",
                         precorConnectApiBaseUrl
                )
                        .isNotNull()
                        .thenGetValue();

    }

    /*
    getter methods
     */
    @Override
    public URL getPrecorConnectApiBaseUrl() {
        return precorConnectApiBaseUrl;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClaimSpiffServiceAdapterConfigImpl that = (ClaimSpiffServiceAdapterConfigImpl) o;

        return precorConnectApiBaseUrl.equals(that.precorConnectApiBaseUrl);

    }

    @Override
    public int hashCode() {
        return precorConnectApiBaseUrl.hashCode();
    }
}
