package com.precorconnect.consumersaleregistrationservice.claimspiffservice;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.claimspiffservice.objectmodel.SpiffDetailsDto;

public interface AddSpiffDetailsFeature {
	
	void execute(
			@NonNull Collection<SpiffDetailsDto> spiffDetailsList,
			@NonNull OAuth2AccessToken accessToken
			) throws AuthenticationException, AuthorizationException;

}
