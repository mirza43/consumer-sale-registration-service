package com.precorconnect.consumersaleregistrationservice.sdk;

import java.net.URL;

public interface ConsumerSaleRegistrationServiceConfig {
  
    URL getPrecorConnectApiBaseUrl();

}
