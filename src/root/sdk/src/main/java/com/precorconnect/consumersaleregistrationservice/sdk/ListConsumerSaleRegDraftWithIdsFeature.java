package com.precorconnect.consumersaleregistrationservice.sdk;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.AuthorizationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.webapi.ConsumerSaleRegSynopsisView;

public interface ListConsumerSaleRegDraftWithIdsFeature {
	
	Collection<ConsumerSaleRegSynopsisView> execute(
	            Collection<ConsumerSaleRegDraftId> consumerSaleRegDraftIds,
	            @NonNull OAuth2AccessToken accessToken
	    		) throws AuthenticationException, AuthorizationException;

}
