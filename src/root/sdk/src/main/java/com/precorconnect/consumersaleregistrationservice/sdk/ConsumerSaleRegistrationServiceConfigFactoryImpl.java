package com.precorconnect.consumersaleregistrationservice.sdk;

import java.net.MalformedURLException;
import java.net.URL;


public class ConsumerSaleRegistrationServiceConfigFactoryImpl
        implements ConsumerSaleRegistrationServiceConfigFactory {

    @Override
    public ConsumerSaleRegistrationServiceConfig construct() {

        return
                new ConsumerSaleRegistrationServiceConfigImpl(
                		constructprecorConnectApiBaseUrl()
                );

    }
    
    public URL constructprecorConnectApiBaseUrl(){

		try {

            String precorConnectApiBaseUrl=System
            		            .getenv("PRECOR_CONNECT_API_BASE_URL");

            return
            		new URL(
            		  precorConnectApiBaseUrl
            		);

        } catch (MalformedURLException e) {

            throw new RuntimeException(e);

        }
}
}
