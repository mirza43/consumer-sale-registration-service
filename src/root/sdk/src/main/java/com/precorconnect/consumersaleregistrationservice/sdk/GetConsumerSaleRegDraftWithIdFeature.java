package com.precorconnect.consumersaleregistrationservice.sdk;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.webapi.ConsumerSaleRegSynopsisView;

public interface GetConsumerSaleRegDraftWithIdFeature {

	ConsumerSaleRegSynopsisView execute(
	            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId,
	            @NonNull OAuth2AccessToken accessToken
	    ) throws AuthenticationException;

}
