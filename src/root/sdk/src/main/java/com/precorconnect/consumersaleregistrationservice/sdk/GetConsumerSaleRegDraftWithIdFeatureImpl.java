package com.precorconnect.consumersaleregistrationservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.google.inject.Singleton;
import com.precorconnect.AuthenticationException;
import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.webapi.ConsumerSaleRegSynopsisView;

@Singleton
public class GetConsumerSaleRegDraftWithIdFeatureImpl 
		implements GetConsumerSaleRegDraftWithIdFeature {

	/*
    fields
     */
    private final WebTarget consumerSaleRegDraftWebTarget;
    
    /*
    constructors
     */
    @Inject
    public GetConsumerSaleRegDraftWithIdFeatureImpl(
    		@NonNull final WebTarget consumerSaleRegDraftWebTarget
    ) {

    	this.consumerSaleRegDraftWebTarget =
                guardThat(
                        "consumerSaleRegDraftWebTarget",
                        consumerSaleRegDraftWebTarget
                )
                        .isNotNull()
                        .thenGetValue().path("/consumer-sale-registration");
    	
    }

	@Override
	public ConsumerSaleRegSynopsisView execute(
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull OAuth2AccessToken accessToken
			)  throws AuthenticationException {

		String authorizationHeaderValue =
                String.format(
                        "Bearer %s",
                        accessToken.getValue()
                );

		
		ConsumerSaleRegSynopsisView consumerPartnerSaleRegDraftView;
        try {
        	consumerPartnerSaleRegDraftView =
        			consumerSaleRegDraftWebTarget
        					.path("/draftId/"+ String.format(
                                            "%s",
                                            consumerSaleRegDraftId.getValue()
                                    ))
                            .request(MediaType.APPLICATION_JSON_TYPE)
                            .header("Authorization", authorizationHeaderValue)
                            .get(ConsumerSaleRegSynopsisView.class);
                            
        	

        } catch (NotAuthorizedException e) {

        	throw new AuthenticationException(e);

        }

        return
        		consumerPartnerSaleRegDraftView;
                
	}
}
