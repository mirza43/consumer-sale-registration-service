package com.precorconnect.consumersaleregistrationservice.sdk;

public interface ConsumerSaleRegistrationServiceConfigFactory {

	ConsumerSaleRegistrationServiceConfig construct();   
}
