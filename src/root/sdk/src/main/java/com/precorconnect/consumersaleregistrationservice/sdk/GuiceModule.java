package com.precorconnect.consumersaleregistrationservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URISyntaxException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.glassfish.jersey.apache.connector.ApacheClientProperties;
import org.glassfish.jersey.apache.connector.ApacheConnectorProvider;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.jackson.JacksonFeature;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

class GuiceModule extends AbstractModule {

    /*
    fields
     */
    private final ConsumerSaleRegistrationServiceConfig config;

    /*
    constructors
     */
    @Inject
    public GuiceModule(
            @NonNull final ConsumerSaleRegistrationServiceConfig config
    ) {

    	this.config =
                guardThat(
                        "config",
                         config
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    protected void configure(
    ) {

        bindFactories();

        bindFeatures();

    }

    private void bindFactories() {

    	/*bind(ConsumerSaleRegDraftViewFactory.class)
				.to(ConsumerSaleRegDraftViewFactoryImpl.class);*/
    }

    private void bindFeatures() {

    	bind(GetConsumerSaleRegDraftWithIdFeature.class)
				.to(GetConsumerSaleRegDraftWithIdFeatureImpl.class);
    	
    	bind(ListConsumerSaleRegDraftWithIdsFeature.class)
    			.to(ListConsumerSaleRegDraftWithIdsFeatureImpl.class);

    }

    @Provides
    @Singleton
    WebTarget constructWebTarget(
            @NonNull final ObjectMapperProvider objectMapperProvider
    ) {

    	 try {
         	ClientConfig clientConfig = new ClientConfig();
         	clientConfig.property(ApacheClientProperties.CONNECTION_MANAGER, new PoolingHttpClientConnectionManager());
         	clientConfig.connectorProvider(new ApacheConnectorProvider());

             return
                     ClientBuilder
                             .newClient(clientConfig)
                             .property(ClientProperties.CONNECT_TIMEOUT, 50000)
                             .property(ClientProperties.READ_TIMEOUT,    50000)
                             .register(objectMapperProvider)
                             .register(JacksonFeature.class)
                             .target(config.getPrecorConnectApiBaseUrl().toURI());

         } catch (URISyntaxException e) {

             throw new RuntimeException(e);

         }
    }

}
