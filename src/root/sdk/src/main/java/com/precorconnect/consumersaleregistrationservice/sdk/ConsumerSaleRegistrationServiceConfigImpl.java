package com.precorconnect.consumersaleregistrationservice.sdk;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.net.URL;

import org.checkerframework.checker.nullness.qual.NonNull;



public class ConsumerSaleRegistrationServiceConfigImpl
        implements ConsumerSaleRegistrationServiceConfig {

    /*
    fields
     */
   
    
    private final URL precorConnectApiBaseUrl;
    
    /*
    constructors
    */
    public ConsumerSaleRegistrationServiceConfigImpl(
    		 @NonNull final URL precorConnectApiBaseUrl
    ) {

    	this.precorConnectApiBaseUrl =
                guardThat(
                        "precorConnectApiBaseUrl",
                        precorConnectApiBaseUrl
                )
                        .isNotNull()
                        .thenGetValue();
    }

    /*
    getter methods
     */
    @Override
	public URL getPrecorConnectApiBaseUrl() {
		return precorConnectApiBaseUrl;
	}
    
    /*
    equality methods
    */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((precorConnectApiBaseUrl == null) ? 0 : precorConnectApiBaseUrl.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerSaleRegistrationServiceConfigImpl other = (ConsumerSaleRegistrationServiceConfigImpl) obj;
		if (precorConnectApiBaseUrl == null) {
			if (other.precorConnectApiBaseUrl != null)
				return false;
		} else if (!precorConnectApiBaseUrl.equals(other.precorConnectApiBaseUrl))
			return false;
		return true;
	}

}
