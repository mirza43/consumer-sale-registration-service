package com.precorconnect.consumersaleregistrationservice.sdk;

import com.precorconnect.*;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;

import java.net.URI;
import java.net.URISyntaxException;

public class Dummy {

    /*
    fields
     */
    private AccountId accountId =
            new AccountIdImpl("123456781234567899");

    private final FirstName firstName =
            new FirstNameImpl("First Name");

    private final LastName lastName =
            new LastNameImpl("Last Name");

    private final UserId userId =
            new UserIdImpl("00u612sf8vlKu2wpP0h7");

    private final SapVendorNumber sapVendorNumber =
            new SapVendorNumberImpl("0000000000");
    
    public ConsumerSaleRegDraftId getConsumerSaleRegDraftId() {
		return consumerSaleRegDraftId;
	}

	private final ConsumerSaleRegDraftId consumerSaleRegDraftId = 
    		new ConsumerSaleRegDraftIdImpl(1000000L);

    private final URI uri;

    /*
    constructors
     */ {
        try {

            uri = new URI("http://dev.precorconnect.com");

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }
    }

    /*
    getter methods
     */
    public AccountId getAccountId() {
        return accountId;
    }

    public FirstName getFirstName() {
        return firstName;
    }

    public LastName getLastName() {
        return lastName;
    }

    public UserId getUserId() {
        return userId;
    }

    public SapVendorNumber getSapVendorNumber() {
        return sapVendorNumber;
    }

    public URI getUri() {
        return uri;
    }
}
