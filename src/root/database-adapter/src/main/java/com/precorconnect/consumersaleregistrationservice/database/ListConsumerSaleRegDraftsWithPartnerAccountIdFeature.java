package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.AccountId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSynopsisView;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.Collection;

interface ListConsumerSaleRegDraftsWithPartnerAccountIdFeature {

    Collection<ConsumerSaleRegDraftSynopsisView> execute(
            @NonNull AccountId partnerAccountId
    );

}
