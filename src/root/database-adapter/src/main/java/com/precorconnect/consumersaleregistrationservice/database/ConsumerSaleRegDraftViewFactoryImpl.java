package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.CityNameImpl;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.Iso31661Alpha2CodeImpl;
import com.precorconnect.Iso31662CodeImpl;
import com.precorconnect.LastNameImpl;
import com.precorconnect.PostalCodeImpl;
import com.precorconnect.StreetAddressImpl;
import com.precorconnect.consumersaleregistrationservice.AddressLineImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerPhoneNumberImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPostalAddressImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftViewImpl;
import com.precorconnect.consumersaleregistrationservice.InvoiceNumberImpl;
import com.precorconnect.consumersaleregistrationservice.InvoiceUrlImpl;
import com.precorconnect.consumersaleregistrationservice.IsSubmitImpl;
import com.precorconnect.consumersaleregistrationservice.PersonEmailImpl;

@Singleton
class ConsumerSaleRegDraftViewFactoryImpl
        implements ConsumerSaleRegDraftViewFactory {

    /*
    fields
     */
    private final ConsumerSaleRegDraftSaleLineItemFactory consumerSaleRegDraftSaleLineItemFactory;

    /*
    constructors
     */
    @Inject
    public ConsumerSaleRegDraftViewFactoryImpl(
            @NonNull ConsumerSaleRegDraftSaleLineItemFactory consumerSaleRegDraftSaleLineItemFactory
    ) {

    	this.consumerSaleRegDraftSaleLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleLineItemFactory",
                         consumerSaleRegDraftSaleLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public ConsumerPartnerSaleRegDraftView construct(
            @NonNull ConsumerPartnerSaleRegDraft consumerSaleRegDraft
    ) {

        return
                new ConsumerSaleRegDraftViewImpl(
                        new ConsumerSaleRegDraftIdImpl(
                                consumerSaleRegDraft
                                        .getId()
                        ),
                        new FirstNameImpl(
                        		consumerSaleRegDraft
                        		.getFirstName()
                        		),
                        new LastNameImpl(
                        		consumerSaleRegDraft
                        		.getLastName()
                        		),
                        new ConsumerPostalAddressImpl(
                        		new StreetAddressImpl(
                        				consumerSaleRegDraft.getAddressLine1()
                        				),
                        		new AddressLineImpl(
                                		consumerSaleRegDraft.getAddressLine2()
                                		),		
                        		new CityNameImpl(
                        				consumerSaleRegDraft.getCity()
                        				),
                        		new Iso31662CodeImpl(
                        				consumerSaleRegDraft.getRegion()
                        				),
                        		new PostalCodeImpl(
                        				consumerSaleRegDraft.getPostalCode()
                        				),
                        		new Iso31661Alpha2CodeImpl(
                        				consumerSaleRegDraft.getCountry()
                        				)
                        	),
                      
                        		consumerSaleRegDraft
                        		.getPhone()
                        		.map(ConsumerPhoneNumberImpl::new)
                                .orElse(null),
                        
                        		consumerSaleRegDraft
                        		.getEmail()
                        		.map(PersonEmailImpl::new)
                                .orElse(null),
                        
                        new AccountIdImpl(
                                consumerSaleRegDraft
                                        .getPartnerAccountId()
                        ),
                        consumerSaleRegDraft
                                .getSaleLineItems()
                                .stream()
                                .map(consumerSaleRegDraftSaleLineItemFactory::construct)
                                .collect(Collectors.toList()),
                                
                        consumerSaleRegDraft
                                .getSellDate(),
                        new IsSubmitImpl(
                        		consumerSaleRegDraft
                                	.getIsSubmitted()
                        ),
                        
                        consumerSaleRegDraft
                        		.getInvoiceUrl()
                        		.map(InvoiceUrlImpl::new)
                        		.orElse(null),
                       
                        new InvoiceNumberImpl(
                        		consumerSaleRegDraft
                                	.getInvoiceNumber()
                        ),
                        
                        consumerSaleRegDraft
                                .getSaleCreatedAtTimestamp()
                                .orElse(null),
                                
                        consumerSaleRegDraft
                                .getSalePartnerRepId()
                                .map(com.precorconnect.UserIdImpl::new)
                                .orElse(null),
                                
                         consumerSaleRegDraft
                         			.getCreatedDate()
                		);

    }
}
