package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;


@Singleton
class ListConsumerSaleRegDraftWithIdsFeatureImpl
        implements ListConsumerSaleRegDraftWithIdsFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final ConsumerSaleRegDraftViewFactory consumerSaleRegDraftViewFactory;

    /*
    constructors
     */
    @Inject
    public ListConsumerSaleRegDraftWithIdsFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull ConsumerSaleRegDraftViewFactory consumerSaleRegDraftViewFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerSaleRegDraftViewFactory =
                guardThat(
                        "consumerSaleRegDraftViewFactory",
                        consumerSaleRegDraftViewFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @SuppressWarnings("unchecked")
	@Override
    public Collection<ConsumerPartnerSaleRegDraftView> execute(
            @NonNull Collection<ConsumerSaleRegDraftId> consumerSaleRegDraftIds
    ) {

        Session session = null;

        try {

            session = sessionFactory.openSession();

           /* ConsumerPartnerSaleRegDraft consumerPartnerSaleRegDraft =
                    (ConsumerPartnerSaleRegDraft) session.get(
                    		ConsumerPartnerSaleRegDraft.class,
                            consumerSaleRegDraftId.getValue()
                    );*/
            

            List<Long> draftIds = consumerSaleRegDraftIds
           		 				.stream()
           		 				.map(draftId->draftId.getValue())
           		 				.collect(Collectors.toList());
            
            Collection<ConsumerPartnerSaleRegDraft> consumerPartnerSaleRegDraft =
                    session
                    		.createQuery("from ConsumerPartnerSaleRegDraft id where id.id in (:consumerSaleRegDraftIds)")
                    		.setParameterList("consumerSaleRegDraftIds", draftIds)
                    		.list();


            return
            		consumerPartnerSaleRegDraft
            		.stream()
            		.map(consumerSaleRegDraftViewFactory :: construct)
                    .collect(Collectors.toList());
                            

        } finally {

            if (null != session) {
                session.close();
            }

        }

    }
}
