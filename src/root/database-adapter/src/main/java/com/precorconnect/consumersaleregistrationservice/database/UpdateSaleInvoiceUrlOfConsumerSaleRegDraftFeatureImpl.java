
package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
/*
    fields
     */
/*
    constructors
     */
@Singleton
class UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeatureImpl
        implements UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;

    /*
    constructors
     */
    @Inject
    public UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeatureImpl(
            @NonNull SessionFactory sessionFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public void execute(
             com.precorconnect.consumersaleregistrationservice.
             @NonNull UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request
    ) {

        Session session = null;

        try {

            session = sessionFactory.openSession();

            try {

                session.beginTransaction();
                
               ConsumerPartnerSaleRegDraft consumerSaleRegDraft =
                        (ConsumerPartnerSaleRegDraft) session.get(
                        		ConsumerPartnerSaleRegDraft.class,
                                request.getConsumerSaleRegDraftId().getValue()
                        );

                consumerSaleRegDraft
                        .setInvoiceUrl(
                                request
                                        .getInvoiceUrl()
                                        .getValue()
                        );

                session.save(consumerSaleRegDraft);

                session.getTransaction().commit();

            } catch (HibernateException e) {

                session.getTransaction().rollback();
                throw e;

            }

        } finally {

            if (null != session) {
                session.close();
            }

        }

    }
}
