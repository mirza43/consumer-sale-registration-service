package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegistrationConstants;

class DeleteConsumerSaleRegDraftWithIdFeatureImpl
        implements DeleteConsumerSaleRegDraftWithIdFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;

    /*
    constructors
     */
    @Inject
    public DeleteConsumerSaleRegDraftWithIdFeatureImpl(
            @NonNull SessionFactory sessionFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public String execute(
            @NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId
    ) {

        Session session = null;

        try {

            session = sessionFactory.openSession();

            try {

                session.beginTransaction();

                ConsumerSaleRegDraft consumerSaleRegDraft =
                        (ConsumerSaleRegDraft) session.load(
                        		ConsumerSaleRegDraft.class,
                                consumerSaleRegDraftId.getValue()
                        );

                session.delete(consumerSaleRegDraft);

                session.getTransaction().commit();

            } catch (HibernateException e) {

                session.getTransaction().rollback();
                throw new RuntimeException(e);

            }

        } finally {

            if (null != session) {
                session.close();
            }

        }
        
        return ConsumerSaleRegistrationConstants.DELETE_CONSUMER_DRAFT_SUCCESS;

    }
}
