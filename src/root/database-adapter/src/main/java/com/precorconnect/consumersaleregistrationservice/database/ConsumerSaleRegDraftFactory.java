package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.AddConsumerSaleRegDraftReq;
import org.checkerframework.checker.nullness.qual.NonNull;

interface ConsumerSaleRegDraftFactory {

    ConsumerPartnerSaleRegDraft construct(
            @NonNull AddConsumerSaleRegDraftReq addConsumerSaleRegDraftReq
    );

}
