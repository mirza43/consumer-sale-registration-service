package com.precorconnect.consumersaleregistrationservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.SerialNumber;

public interface ConsumerSaleRegDraftViewWithSerialNumberExclusionFactory {
	
	ConsumerPartnerSaleRegDraftView construct(
            @NonNull ConsumerPartnerSaleRegDraft consumerPartnerSaleRegDraft,
            @NonNull Collection<SerialNumber> serialNumberExclusionList
    );

}
