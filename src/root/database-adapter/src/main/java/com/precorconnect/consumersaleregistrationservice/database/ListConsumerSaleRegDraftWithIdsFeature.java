package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

interface ListConsumerSaleRegDraftWithIdsFeature {

	Collection<ConsumerPartnerSaleRegDraftView> execute(
            @NonNull Collection<ConsumerSaleRegDraftId> ConsumerSaleRegDraftIds
    );

}
