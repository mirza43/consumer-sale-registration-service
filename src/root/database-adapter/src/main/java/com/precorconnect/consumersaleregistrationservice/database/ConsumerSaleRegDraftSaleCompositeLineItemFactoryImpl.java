package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleCompositeLineItemImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItemImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;



@Singleton
class ConsumerSaleRegDraftSaleCompositeLineItemFactoryImpl
        implements ConsumerSaleRegDraftSaleCompositeLineItemFactory {

    /*
    fields
     */
    private final ConsumerSaleLineItemComponentFactory partnerSaleLineItemComponentFactory;

    /*
    constructors
     */
    @Inject
    public ConsumerSaleRegDraftSaleCompositeLineItemFactoryImpl(
            @NonNull ConsumerSaleLineItemComponentFactory partnerSaleLineItemComponentFactory
    ) {

    	this.partnerSaleLineItemComponentFactory =
                guardThat(
                        "partnerSaleLineItemComponentFactory",
                         partnerSaleLineItemComponentFactory
                )
                        .isNotNull()
                        .thenGetValue();


    }

    @Override
    public ConsumerSaleRegDraftSaleCompositeLineItem construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
    ) {

        ConsumerSaleRegDraftSaleCompositeLineItem objectUnderTest =
                new ConsumerSaleRegDraftSaleCompositeLineItem();
        
        objectUnderTest.setId(
        		consumerSaleRegDraftSaleCompositeLineItem
        		.getId()
        		.getValue()
        		);

        objectUnderTest
                .setComponents(
                        StreamSupport
                                .stream(
                                        consumerSaleRegDraftSaleCompositeLineItem
                                                .getComponents()
                                                .spliterator(),
                                        false)
                                .map(partnerSaleLineItemComponentFactory::construct)
                                .collect(Collectors.toList())
                );

        objectUnderTest
                .setPrice(
                        consumerSaleRegDraftSaleCompositeLineItem
                                .getPrice()
                                .orElse(null)
                );

        return objectUnderTest;

    }

    @Override
    public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem construct(
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
    ) {

        return
                new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
                        new ConsumerSaleRegDraftSaleLineItemIdImpl(
                                consumerSaleRegDraftSaleCompositeLineItem.getId()
                        ),
                        new ConsumerSaleCompositeLineItemImpl(
                                consumerSaleRegDraftSaleCompositeLineItem
                                        .getComponents()
                                        .stream()
                                        .map(partnerSaleLineItemComponentFactory::construct)
                                        .collect(Collectors.toList()),
                                consumerSaleRegDraftSaleCompositeLineItem
                                        .getPrice()
                                        .orElse(null)
                        )
                );

    }

}
