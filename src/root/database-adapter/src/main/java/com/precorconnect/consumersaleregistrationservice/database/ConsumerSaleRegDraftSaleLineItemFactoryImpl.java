package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItem;

@Singleton
class ConsumerSaleRegDraftSaleLineItemFactoryImpl
        implements ConsumerSaleRegDraftSaleLineItemFactory {

    /*
    fields
     */
    private final ConsumerSaleRegDraftSaleSimpleLineItemFactory consumerSaleRegDraftSaleSimpleLineItemFactory;

    private final ConsumerSaleRegDraftSaleCompositeLineItemFactory consumerSaleRegDraftSaleCompositeLineItemFactory;

    /*
    constructors
     */
    @Inject
    public ConsumerSaleRegDraftSaleLineItemFactoryImpl(
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItemFactory partnerSaleRegDraftSaleSimpleLineItemFactory,
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItemFactory partnerSaleRegDraftSaleCompositeLineItemFactory
    ) {

    	this.consumerSaleRegDraftSaleSimpleLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleSimpleLineItemFactory",
                         partnerSaleRegDraftSaleSimpleLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerSaleRegDraftSaleCompositeLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleCompositeLineItemFactory",
                         partnerSaleRegDraftSaleCompositeLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();
    }

    @Override
    public ConsumerSaleRegDraftSaleLineItem construct(
            @NonNull ConsumerSaleLineItem consumerSaleLineItem
    ) {

        if (consumerSaleLineItem instanceof
                com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem) {
            return
                    consumerSaleRegDraftSaleSimpleLineItemFactory
                            .construct(
                                    (com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem)
                                            consumerSaleLineItem
                            );
        } else if (consumerSaleLineItem instanceof
                com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem) {
            return
                    consumerSaleRegDraftSaleCompositeLineItemFactory
                            .construct(
                                    (com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem)
                                            consumerSaleLineItem
                            );
        }

        // handle unexpected implementations
        throw
                new RuntimeException(
                        String.format(
                                "received unsupported %s implementation: %s",
                                com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem.class,
                                consumerSaleLineItem.getClass()
                        )
                );

    }

    @Override
    public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem construct(
            @NonNull ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem
    ) {

        if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleSimpleLineItem) {

            return
                    consumerSaleRegDraftSaleSimpleLineItemFactory
                            .construct(
                                    (ConsumerSaleRegDraftSaleSimpleLineItem) consumerSaleRegDraftSaleLineItem
                            );

        } else if (consumerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleCompositeLineItem) {

            return
                    consumerSaleRegDraftSaleCompositeLineItemFactory
                            .construct(
                                    (ConsumerSaleRegDraftSaleCompositeLineItem) consumerSaleRegDraftSaleLineItem
                            );

        }
        // handle unexpected implementations
        throw
                new RuntimeException(
                        String.format(
                                "received unsupported %s implementation: %s",
                                ConsumerSaleRegDraftSaleLineItem.class,
                                consumerSaleRegDraftSaleLineItem.getClass()
                        )
                );

    }

	
}
