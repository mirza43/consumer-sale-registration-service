package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateConsumerSaleLineItemComponentReqFactory {
	
	ConsumerSaleLineItemComponent construct(
			
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleLineItemComponent consumerSaleLineItemComponent
    );

}
