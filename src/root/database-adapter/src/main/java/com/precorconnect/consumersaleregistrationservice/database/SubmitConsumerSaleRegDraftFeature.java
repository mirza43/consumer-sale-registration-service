package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.OAuth2AccessToken;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;

public interface SubmitConsumerSaleRegDraftFeature {

	ConsumerPartnerSaleRegDraftView execute(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId,
            @NonNull OAuth2AccessToken accessToken
    );

}
