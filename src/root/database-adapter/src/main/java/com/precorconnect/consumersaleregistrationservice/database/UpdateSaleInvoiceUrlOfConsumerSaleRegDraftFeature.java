package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;


interface UpdateSaleInvoiceUrlOfConsumerSaleRegDraftFeature {

    void execute(
             com.precorconnect.consumersaleregistrationservice. @NonNull UpdateSaleInvoiceUrlOfConsumerSaleRegDraftReq request
    );

}
