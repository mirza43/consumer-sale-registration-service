package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;



@Singleton
public class UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactoryImpl
		implements UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactory {
	
    
    private final UpdateConsumerSaleLineItemComponentReqFactory updatePartnerSaleLineItemComponentReqFactory;
    
    
    
    @Inject
	public UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactoryImpl(	
			@NonNull UpdateConsumerSaleLineItemComponentReqFactory updatePartnerSaleLineItemComponentReqFactory) {
		
		this.updatePartnerSaleLineItemComponentReqFactory =
                guardThat(
                        "updatePartnerSaleLineItemComponentReqFactory",
                        updatePartnerSaleLineItemComponentReqFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		
	}




	@Override
	public com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleCompositeLineItem construct(
			 com.precorconnect.consumersaleregistrationservice.
			 @NonNull ConsumerSaleRegDraftSaleCompositeLineItem partnerSaleRegDraftSaleCompositeLineItem) {

		
		try {
		                
		    com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleCompositeLineItem objectUnderConstruction=
		    		new com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleCompositeLineItem();
        			
		    objectUnderConstruction
	        .setId(
	        		partnerSaleRegDraftSaleCompositeLineItem
	        		.getId()
	        		.getValue()
	        	);
                        
		    objectUnderConstruction
            .setComponents(
                    StreamSupport
                            .stream(
                                    partnerSaleRegDraftSaleCompositeLineItem
                                            .getComponents()
                                            .spliterator(),
                                    false)
                            .map(updatePartnerSaleLineItemComponentReqFactory::construct)
                            .collect(Collectors.toList())
            );

		    objectUnderConstruction
            .setPrice(
                    partnerSaleRegDraftSaleCompositeLineItem
                            .getPrice()
                            .orElse(null)
            );
		   
	           return objectUnderConstruction;

	        } catch(final Exception e) {
	        	e.printStackTrace();	
	        	throw new RuntimeException("exception while constructing PartnerSaleRegDraftSaleCompositeLineItem:", e);

	        } 
	}

}
