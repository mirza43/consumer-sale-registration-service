package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.CityNameImpl;
import com.precorconnect.FirstNameImpl;
import com.precorconnect.Iso31661Alpha2CodeImpl;
import com.precorconnect.Iso31662CodeImpl;
import com.precorconnect.LastNameImpl;
import com.precorconnect.PostalCodeImpl;
import com.precorconnect.StreetAddressImpl;
import com.precorconnect.consumersaleregistrationservice.AddressLineImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerPhoneNumberImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPostalAddressImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftViewImpl;
import com.precorconnect.consumersaleregistrationservice.InvoiceNumberImpl;
import com.precorconnect.consumersaleregistrationservice.InvoiceUrlImpl;
import com.precorconnect.consumersaleregistrationservice.IsSubmitImpl;
import com.precorconnect.consumersaleregistrationservice.PersonEmailImpl;
import com.precorconnect.consumersaleregistrationservice.SerialNumber;

@Singleton
public class ConsumerSaleRegDraftViewWithSerialNumberExclusionFactoryImpl
		implements ConsumerSaleRegDraftViewWithSerialNumberExclusionFactory {
	
	
	 /*
    fields
     */
    private final ConsumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory consumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory;

    /*
    constructors
     */
    @Inject
    public ConsumerSaleRegDraftViewWithSerialNumberExclusionFactoryImpl(
            @NonNull ConsumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory consumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory
    ) {

    	this.consumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory =
                guardThat(
                        "consumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory",
                        consumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }
	
	

	@Override
	public ConsumerPartnerSaleRegDraftView construct(
			@NonNull ConsumerPartnerSaleRegDraft consumerSaleRegDraft,
			@NonNull Collection<SerialNumber> serialNumberExclusionList
			) {
		
		Collection<com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleLineItem> saleLineItems=consumerSaleRegDraft.getSaleLineItems();
		
		Collection<ConsumerSaleRegDraftSaleLineItem> consumerSaleLineItems=new ArrayList<>();
		
		
		for(com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem:saleLineItems){
			
			consumerSaleLineItems.add(
					consumerSaleRegDraftSaleLineItemWithSerialNumberExclusionFactory.construct(
							consumerSaleRegDraftSaleLineItem,
							serialNumberExclusionList
							)
					);
			
		}
		 
		
		return
                new ConsumerSaleRegDraftViewImpl(
                        new ConsumerSaleRegDraftIdImpl(
                                consumerSaleRegDraft
                                        .getId()
                        ),
                        new FirstNameImpl(
                        		consumerSaleRegDraft
                        		.getFirstName()
                        		),
                        new LastNameImpl(
                        		consumerSaleRegDraft
                        		.getLastName()
                        		),
                        new ConsumerPostalAddressImpl(
                        		new StreetAddressImpl(
                        				consumerSaleRegDraft.getAddressLine1()
                        				),
                        		new AddressLineImpl(
                                		consumerSaleRegDraft.getAddressLine2()
                                		),		
                        		new CityNameImpl(
                        				consumerSaleRegDraft.getCity()
                        				),
                        		new Iso31662CodeImpl(
                        				consumerSaleRegDraft.getRegion()
                        				),
                        		new PostalCodeImpl(
                        				consumerSaleRegDraft.getPostalCode()
                        				),
                        		new Iso31661Alpha2CodeImpl(
                        				consumerSaleRegDraft.getCountry()
                        				)
                        	),
                      
                        		consumerSaleRegDraft
                        		.getPhone()
                        		.map(ConsumerPhoneNumberImpl::new)
                                .orElse(null),
                        
                        		consumerSaleRegDraft
                        		.getEmail()
                        		.map(PersonEmailImpl::new)
                                .orElse(null),
                        
                        new AccountIdImpl(
                                consumerSaleRegDraft
                                        .getPartnerAccountId()
                        ),
                        consumerSaleLineItems,
                                
                        consumerSaleRegDraft
                                .getSellDate(),
                        new IsSubmitImpl(
                        		consumerSaleRegDraft
                                	.getIsSubmitted()
                        ),
                        
                        consumerSaleRegDraft
                        		.getInvoiceUrl()
                        		.map(InvoiceUrlImpl::new)
                        		.orElse(null),
                       
                        new InvoiceNumberImpl(
                        		consumerSaleRegDraft
                                	.getInvoiceNumber()
                        ),
                        
                        consumerSaleRegDraft
                                .getSaleCreatedAtTimestamp()
                                .orElse(null),
                                
                        consumerSaleRegDraft
                                .getSalePartnerRepId()
                                .map(com.precorconnect.UserIdImpl::new)
                                .orElse(null),
                                
                         consumerSaleRegDraft
                         			.getCreatedDate()
                		);
	}

}
