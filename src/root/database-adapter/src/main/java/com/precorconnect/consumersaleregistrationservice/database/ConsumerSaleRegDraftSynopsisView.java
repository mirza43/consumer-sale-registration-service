package com.precorconnect.consumersaleregistrationservice.database;

import org.hibernate.annotations.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name = "ConsumerSaleRegDrafts")
@Inheritance(strategy = InheritanceType.JOINED)
class ConsumerSaleRegDraftSynopsisView {

    /*
    fields
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String partnerAccountId;

    private String saleAccountId;

    /*
    getter & setter methods
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPartnerAccountId() {
        return partnerAccountId;
    }

    public void setPartnerAccountId(String partnerAccountId) {
        this.partnerAccountId = partnerAccountId;
    }

    public String getSaleAccountId() {
        return saleAccountId;
    }

    public void setSaleAccountId(String saleAccountId) {
        this.saleAccountId = saleAccountId;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConsumerSaleRegDraftSynopsisView that = (ConsumerSaleRegDraftSynopsisView) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (partnerAccountId != null ? !partnerAccountId.equals(that.partnerAccountId) : that.partnerAccountId != null)
            return false;
        return !(saleAccountId != null ? !saleAccountId.equals(that.saleAccountId) : that.saleAccountId != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (partnerAccountId != null ? partnerAccountId.hashCode() : 0);
        result = 31 * result + (saleAccountId != null ? saleAccountId.hashCode() : 0);
        return result;
    }
}
