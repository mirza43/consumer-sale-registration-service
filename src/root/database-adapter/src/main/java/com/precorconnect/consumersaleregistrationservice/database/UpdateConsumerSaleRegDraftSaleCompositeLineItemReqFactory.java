package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactory {
	
	ConsumerSaleRegDraftSaleCompositeLineItem construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
    );

}
