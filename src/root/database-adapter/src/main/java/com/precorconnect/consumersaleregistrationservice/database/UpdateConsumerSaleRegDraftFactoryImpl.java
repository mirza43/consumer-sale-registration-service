package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.sql.Timestamp;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.UpdateConsumerSaleRegDraftReq;

@Singleton
public class UpdateConsumerSaleRegDraftFactoryImpl implements UpdateConsumerSaleRegDraftFactory {

	private UpdateConsumerSaleRegDraftSaleLineItemFactory updateConsumerSaleRegDraftSaleLineItemFactory;




	@Inject
	public UpdateConsumerSaleRegDraftFactoryImpl(
			UpdateConsumerSaleRegDraftSaleLineItemFactory updateConsumerSaleRegDraftSaleLineItemFactory
			) {

		this.updateConsumerSaleRegDraftSaleLineItemFactory =
                guardThat(
                        "updateConsumerSaleRegDraftSaleLineItemFactory",
                        updateConsumerSaleRegDraftSaleLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();

	}




	@Override
	public ConsumerPartnerSaleRegDraft construct(
			@NonNull UpdateConsumerSaleRegDraftReq updateConsumerSaleRegDraftReq,
			@NonNull ConsumerPartnerSaleRegDraft objectUnderConstruction
			) {
		
			 objectUnderConstruction.setFirstName(
					 updateConsumerSaleRegDraftReq
		        		.getFirstName()
		        		.getValue()
					);
			
			objectUnderConstruction.setLastName(
					updateConsumerSaleRegDraftReq
					.getLastName()
					.getValue()
				);
			
			objectUnderConstruction.setAddressLine1(
					updateConsumerSaleRegDraftReq
			                     .getAddress()
			                     .getAddressLine1()
			                     .getValue()
			         				);
			
			objectUnderConstruction.setAddressLine2(
					updateConsumerSaleRegDraftReq
			                     .getAddress()
			                     .getAddressLine2()
			                     .getValue()
			         				);
			
			objectUnderConstruction.setCity(
					updateConsumerSaleRegDraftReq
			     .getAddress()
			     .getCity()
			     .getValue()
					);
			
			objectUnderConstruction.setRegion(
					updateConsumerSaleRegDraftReq
			     .getAddress()
			     .getRegionIso31662Code()
			     .getValue()
					);
			
			objectUnderConstruction.setPostalCode(
					updateConsumerSaleRegDraftReq
			                 .getAddress()
			                 .getPostalCode()
			                 .getValue()
			         );
			
			objectUnderConstruction.setCountry(
					updateConsumerSaleRegDraftReq
			         .getAddress()
			         .getCountryIso31661Alpha2Code()
			         .getValue()
			        );
			 if(updateConsumerSaleRegDraftReq
	        			.getPhoneNumber() != null){
			objectUnderConstruction.setPhone(
					updateConsumerSaleRegDraftReq
			         .getPhoneNumber()
			         .getValue()
						);
			 }
			 
			 if(updateConsumerSaleRegDraftReq
	        			.getPersonEmail() != null){
			objectUnderConstruction.setEmail(
			updateConsumerSaleRegDraftReq
				 .getPersonEmail()
				 .getValue()
				 );
			 }

	        objectUnderConstruction.setPartnerAccountId(
	        		updateConsumerSaleRegDraftReq
	                        .getPartnerAccountId()
	                        .getValue()
	        );

	        objectUnderConstruction.setInvoiceNumber(
	        		updateConsumerSaleRegDraftReq
	        			.getInvoiceNumber()
	        			.getValue()
	        		);

	        if(updateConsumerSaleRegDraftReq
	        			.getInvoiceUrl() != null){
	        objectUnderConstruction.setInvoiceUrl(
	        		updateConsumerSaleRegDraftReq
	        			.getInvoiceUrl()
	        			.getValue()
	        		);
	        }else{
	        	objectUnderConstruction.setInvoiceUrl(null);
	        }

	        objectUnderConstruction.setIsSubmitted(
	        		updateConsumerSaleRegDraftReq
	        			.getIsSubmitted()
	        			.getValue()
	        		);

	        objectUnderConstruction.setSaleCreatedAtTimestamp(
	        		new InstantPersistenceConverter()
	        		.convertToEntityAttribute(new Timestamp(new Date().getTime()))
	        		);



	        objectUnderConstruction.setSaleLineItems(
	        		StreamSupport
	                	.stream(
	                			updateConsumerSaleRegDraftReq
	            				.getSaleLineItems()
	                                .spliterator(),
	                        false
	                     )
	        			.map(updateConsumerSaleRegDraftSaleLineItemFactory :: construct)
	        			.collect(Collectors.toList())
	        		);

	        if(updateConsumerSaleRegDraftReq
	        			.getUserId() != null){
	        objectUnderConstruction.setSalePartnerRepId(
	        		updateConsumerSaleRegDraftReq
	        			.getUserId()
	        			.getValue()
	        		);

	        }

	 
	        	objectUnderConstruction.setSellDate(
	        			updateConsumerSaleRegDraftReq
	        				.getSellDate()
	        				.getValue()
	        				);
	  
	        
	        objectUnderConstruction.setCreatedDate(
        			updateConsumerSaleRegDraftReq
        				.getCreateDate()
        				.getValue()
        				);

	        return objectUnderConstruction;


	}

}
