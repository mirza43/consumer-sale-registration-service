package com.precorconnect.consumersaleregistrationservice.database;

import java.net.URI;
import java.net.URISyntaxException;

import com.precorconnect.Password;
import com.precorconnect.PasswordImpl;
import com.precorconnect.Username;
import com.precorconnect.UsernameImpl;

public class DatabaseAdapterConfigFactoryImpl
        implements DatabaseAdapterConfigFactory {

    @Override
    public DatabaseAdapterConfig construct() {

        return new DatabaseAdapterConfigImpl(
                constructUri(),
                constructUsername(),
                constructPassword()
        );

    }

    private URI constructUri() {

        String baseUri = System.getenv("DATABASE_URI");

        try {

            return new URI(baseUri);

        } catch (URISyntaxException e) {

            throw new RuntimeException(e);

        }

    }

    private Username constructUsername() {

        String username = System.getenv("DATABASE_USERNAME");

        return new UsernameImpl(username);

    }

    private Password constructPassword() {

        String password = System.getenv("DATABASE_PASSWORD");

        return new PasswordImpl(password);

    }    
   
}
