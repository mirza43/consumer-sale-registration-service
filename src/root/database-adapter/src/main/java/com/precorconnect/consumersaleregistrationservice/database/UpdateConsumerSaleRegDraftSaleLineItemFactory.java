package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItem;

public interface UpdateConsumerSaleRegDraftSaleLineItemFactory {
	
	 ConsumerSaleRegDraftSaleLineItem construct(
	            @NonNull ConsumerSaleLineItem partnerSaleLineItem
	    );

	    com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem construct(
	            @NonNull ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem
	    );

}
