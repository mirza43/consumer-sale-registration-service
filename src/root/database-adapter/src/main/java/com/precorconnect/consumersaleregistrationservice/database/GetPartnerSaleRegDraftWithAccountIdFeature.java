package com.precorconnect.consumersaleregistrationservice.database;

import java.util.Collection;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountId;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;

public interface GetPartnerSaleRegDraftWithAccountIdFeature {

	Collection<ConsumerPartnerSaleRegDraftView> execute(
            @NonNull AccountId accountId
    );

}
