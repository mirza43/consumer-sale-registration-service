package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleCompositeLineItemImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItemImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;


@Singleton
public class UpdateConsumerSaleRegDraftSaleCompositeLineItemFactoryImpl
		implements UpdateConsumerSaleRegDraftSaleCompositeLineItemFactory {
	
	 private final UpdateConsumerSaleLineItemComponentFactory partnerSaleLineItemComponentFactory;
	 
	 
	 
	 
	 @Inject
	public UpdateConsumerSaleRegDraftSaleCompositeLineItemFactoryImpl(
			UpdateConsumerSaleLineItemComponentFactory partnerSaleLineItemComponentFactory) {
		
		 
		 this.partnerSaleLineItemComponentFactory =
	                guardThat(
	                        "partnerSaleLineItemComponentFactory",
	                         partnerSaleLineItemComponentFactory
	                )
	                        .isNotNull()
	                        .thenGetValue();

		 
	}





	@Override
	public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem construct(
			com.precorconnect.consumersaleregistrationservice.database.@NonNull ConsumerSaleRegDraftSaleCompositeLineItem partnerSaleRegDraftSaleCompositeLineItem) {
		
		
		return
                new ConsumerSaleRegDraftSaleCompositeLineItemImpl(
                        new ConsumerSaleRegDraftSaleLineItemIdImpl(
                                partnerSaleRegDraftSaleCompositeLineItem.getId()
                        ),
                        new ConsumerSaleCompositeLineItemImpl(
                                partnerSaleRegDraftSaleCompositeLineItem
                                        .getComponents()
                                        .stream()
                                        .map(partnerSaleLineItemComponentFactory::construct)
                                        .collect(Collectors.toList()),
                                partnerSaleRegDraftSaleCompositeLineItem
                                        .getPrice()
                                        .orElse(null)
                        )
                );
		
	}

}
