package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.SerialNumber;

@Singleton
public class GetConsumerSaleDraftExcludeSerialNumberFeatureImpl
		implements GetConsumerSaleDraftExcludeSerialNumberFeature {
	
	 /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final ConsumerSaleRegDraftViewWithSerialNumberExclusionFactory consumerSaleRegDraftViewWithSerialNumberExclusionFactory;

    /*
    constructors
     */
    @Inject
    public GetConsumerSaleDraftExcludeSerialNumberFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull ConsumerSaleRegDraftViewWithSerialNumberExclusionFactory consumerSaleRegDraftViewWithSerialNumberExclusionFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerSaleRegDraftViewWithSerialNumberExclusionFactory =
                guardThat(
                        "consumerSaleRegDraftViewFactory",
                        consumerSaleRegDraftViewWithSerialNumberExclusionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }


	@Override
	public ConsumerPartnerSaleRegDraftView execute(@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId,
			@NonNull Collection<SerialNumber> serialNumberExclusionList) {
		
		Session session = null;

        try {

            session = sessionFactory.openSession();

            ConsumerPartnerSaleRegDraft consumerPartnerSaleRegDraft =
                    (ConsumerPartnerSaleRegDraft) session.get(
                    		ConsumerPartnerSaleRegDraft.class,
                            consumerSaleRegDraftId.getValue()
                    );

            return
            		consumerSaleRegDraftViewWithSerialNumberExclusionFactory
                            .construct(consumerPartnerSaleRegDraft,
                            		serialNumberExclusionList
                            );

        } finally {

            if (null != session) {
                session.close();
            }

        }
		
	}

}
