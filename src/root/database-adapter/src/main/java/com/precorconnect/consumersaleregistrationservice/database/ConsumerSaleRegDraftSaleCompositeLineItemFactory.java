package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

interface ConsumerSaleRegDraftSaleCompositeLineItemFactory {

    ConsumerSaleRegDraftSaleCompositeLineItem construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
    );

    com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem construct(
            @NonNull ConsumerSaleRegDraftSaleCompositeLineItem consumerSaleRegDraftSaleCompositeLineItem
    );

}
