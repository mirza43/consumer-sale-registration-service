package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import org.checkerframework.checker.nullness.qual.NonNull;

interface DeleteConsumerSaleRegDraftWithIdFeature {

    String execute(
            @NonNull ConsumerSaleRegDraftId partnerSaleRegDraftId
    );

}
