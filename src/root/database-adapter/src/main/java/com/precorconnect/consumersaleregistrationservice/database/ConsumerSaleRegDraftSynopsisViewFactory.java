package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

interface ConsumerSaleRegDraftSynopsisViewFactory {

    com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSynopsisView construct(
            @NonNull ConsumerSaleRegDraftSynopsisView consumerSaleRegDraftSynopsisView
    );

}
