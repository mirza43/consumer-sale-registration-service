package com.precorconnect.consumersaleregistrationservice.database;

public interface DatabaseAdapterConfigFactory {

	DatabaseAdapterConfig construct();

}
