package com.precorconnect.consumersaleregistrationservice.database;

import java.net.URI;

import com.precorconnect.Password;
import com.precorconnect.Username;

public interface DatabaseAdapterConfig {

    URI getUri();

    Username getUsername();

    Password getPassword();
   
    

}
