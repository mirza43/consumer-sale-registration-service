package com.precorconnect.consumersaleregistrationservice.database;

import com.precorconnect.consumersaleregistrationservice.AddSaleLineItemToConsumerSaleRegDraftReq;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemId;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItemIdImpl;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
class AddSaleLineItemToConsumerSaleRegDraftFeatureImpl
        implements AddSaleLineItemToConsumerSaleRegDraftFeature {

    /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final ConsumerSaleRegDraftSaleLineItemFactory consumerSaleRegDraftSaleLineItemFactory;

    /*
    constructors
     */
    @Inject
    public AddSaleLineItemToConsumerSaleRegDraftFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull ConsumerSaleRegDraftSaleLineItemFactory consumerSaleRegDraftSaleLineItemFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

    	this.consumerSaleRegDraftSaleLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleLineItemFactory",
                        consumerSaleRegDraftSaleLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

    @Override
    public ConsumerSaleRegDraftSaleLineItemId execute(
            @NonNull AddSaleLineItemToConsumerSaleRegDraftReq request
    ) {

        Session session = null;

        try {

            session = sessionFactory.openSession();

            ConsumerSaleRegDraftSaleLineItem consumerSaleRegDraftSaleLineItem =
                    consumerSaleRegDraftSaleLineItemFactory
                            .construct(
                                    request
                                            .getSaleLineItem()
                            );

            try {

                session.beginTransaction();

                ConsumerSaleRegDraft consumerSaleRegDraft =
                        (ConsumerSaleRegDraft) session
                                .load(
                                		ConsumerSaleRegDraft.class,
                                        request
                                                .getPartnerSaleRegDraftId()
                                                .getValue()
                                );

                // set parent
                consumerSaleRegDraftSaleLineItem
                        .setPartnerSaleRegDraft(
                                consumerSaleRegDraft
                        );

                session.save(consumerSaleRegDraftSaleLineItem);

                session.getTransaction().commit();

            } catch (HibernateException e) {

                session.getTransaction().rollback();
                throw e;

            }

            return

                    new ConsumerSaleRegDraftSaleLineItemIdImpl(
                            consumerSaleRegDraftSaleLineItem.getId()
                    );

        } finally {

            if (null != session) {
                session.close();
            }

        }


    }
}
