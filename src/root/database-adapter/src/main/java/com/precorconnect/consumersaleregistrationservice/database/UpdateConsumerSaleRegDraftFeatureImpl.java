package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftId;
import com.precorconnect.consumersaleregistrationservice.UpdateConsumerSaleRegDraftReq;


@Singleton
public class UpdateConsumerSaleRegDraftFeatureImpl implements UpdateConsumerSaleRegDraftFeature {
	
	  /*
    fields
     */
    private final SessionFactory sessionFactory;

    private final UpdateConsumerSaleRegDraftFactory updateConsumerSaleRegDraftFactory;
    
    private final ConsumerSaleRegDraftViewFactory consumerSaleRegDraftViewFactory;

    /*
    constructors
     */
    @Inject
    public UpdateConsumerSaleRegDraftFeatureImpl(
            @NonNull SessionFactory sessionFactory,
            @NonNull UpdateConsumerSaleRegDraftFactory updateConsumerSaleRegDraftFactory,
            @NonNull ConsumerSaleRegDraftViewFactory consumerSaleRegDraftViewFactory
    ) {

    	this.sessionFactory =
                guardThat(
                        "sessionFactory",
                         sessionFactory
                )
                        .isNotNull()
                        .thenGetValue();

        this.updateConsumerSaleRegDraftFactory =
                guardThat(
                        "partnerCommercialSaleRegDraftFactory",
                        updateConsumerSaleRegDraftFactory
                )
                        .isNotNull()
                        .thenGetValue();
        
        this.consumerSaleRegDraftViewFactory =
                guardThat(
                        "consumerSaleRegDraftViewFactory",
                        consumerSaleRegDraftViewFactory
                )
                        .isNotNull()
                        .thenGetValue();

    }

	@Override
	public ConsumerPartnerSaleRegDraftView execute(
			@NonNull UpdateConsumerSaleRegDraftReq request,
			@NonNull ConsumerSaleRegDraftId consumerSaleRegDraftId
			) {
		Session session = null;
		ConsumerPartnerSaleRegDraft newEntityObject=null;
        try {

            session = sessionFactory.openSession();
            
            ConsumerPartnerSaleRegDraft entityObject = new ConsumerPartnerSaleRegDraft();
            
            entityObject.setId(consumerSaleRegDraftId.getValue());
             	
            ConsumerPartnerSaleRegDraft consumerCommercialSaleRegDraft =
										            		updateConsumerSaleRegDraftFactory
										                    .construct(
										                    		request,
										                    		entityObject
									                    		);

            try {

                session.beginTransaction();                
                session.saveOrUpdate(consumerCommercialSaleRegDraft);
                session.getTransaction().commit();
                newEntityObject=
	                		(ConsumerPartnerSaleRegDraft)session.load(
	                		ConsumerPartnerSaleRegDraft.class,
	                		consumerSaleRegDraftId.getValue()
	                		) ;               

            } catch (HibernateException e) {

                session.getTransaction().rollback();
                throw e;

            }
            
            return
                    consumerSaleRegDraftViewFactory
                            .construct(newEntityObject);

        } finally {

            if (null != session) {
                session.close();
            }

        }
	}

}
