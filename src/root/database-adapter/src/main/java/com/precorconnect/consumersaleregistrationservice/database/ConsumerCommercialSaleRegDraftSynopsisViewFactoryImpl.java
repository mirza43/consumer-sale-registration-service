package com.precorconnect.consumersaleregistrationservice.database;

import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.AccountIdImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftSynopsisView;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSynopsisViewImpl;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftIdImpl;

@Singleton
class ConsumerCommercialSaleRegDraftSynopsisViewFactoryImpl
        implements ConsumerPartnerSaleRegDraftSynopsisViewFactory {

    @Override
    public com.precorconnect.consumersaleregistrationservice.ConsumerPartnerSaleRegDraftSynopsisView construct(
            @NonNull ConsumerPartnerSaleRegDraftSynopsisView consumerCommercialSaleRegDraftSynopsisView
    ) {

        return
                new ConsumerSaleRegDraftSynopsisViewImpl(
                        new ConsumerSaleRegDraftIdImpl(
                                consumerCommercialSaleRegDraftSynopsisView
                                        .getId().getValue()
                        ),
                        new AccountIdImpl(
                                consumerCommercialSaleRegDraftSynopsisView
                                        .getPartnerAccountId().getValue()
                        )
                );

    }
}
