package com.precorconnect.consumersaleregistrationservice.database;

import javax.persistence.*;

import com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleLineItemComponent;
import com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleCompositeLineItem;
import com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleLineItem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Entity
@Table(name = "ConsumerSaleRegDraftSaleCompositeLineItems")
@PrimaryKeyJoinColumn(name = "Id", referencedColumnName = "Id")
class ConsumerSaleRegDraftSaleCompositeLineItem
        extends ConsumerSaleRegDraftSaleLineItem {

    /*
    fields
     */
    private BigDecimal price;

    @OneToMany(
            mappedBy = "consumerSaleRegDraftSaleCompositeLineItem",
            cascade = CascadeType.ALL
    )
    private Collection<ConsumerSaleLineItemComponent> components =
            new ArrayList<>();

    /*
    getter & setter methods
     */
    public Optional<BigDecimal> getPrice() {
        return Optional.ofNullable(price);
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Collection<ConsumerSaleLineItemComponent> getComponents() {
        return components;
    }

    public void setComponents(Collection<ConsumerSaleLineItemComponent> components) {

        components
                .stream()
                .forEach(
                        partnerSaleRegDraftSaleLineItemComponent ->
                                partnerSaleRegDraftSaleLineItemComponent
                                        .setConsumerSaleRegDraftSaleCompositeLineItem(this)
                );

        this.components = components;
    }

    /*
    equality methods
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        ConsumerSaleRegDraftSaleCompositeLineItem that = (ConsumerSaleRegDraftSaleCompositeLineItem) o;

        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        return !(components != null ? !components.equals(that.components) : that.components != null);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (components != null ? components.hashCode() : 0);
        return result;
    }
}
