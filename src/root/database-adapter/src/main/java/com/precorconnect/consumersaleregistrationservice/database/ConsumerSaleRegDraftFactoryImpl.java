package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.AddConsumerSaleRegDraftReq;
import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItem;

@Singleton
class ConsumerSaleRegDraftFactoryImpl
        implements ConsumerSaleRegDraftFactory {

	private ConsumerSaleRegDraftSaleLineItemFactory consumerSaleRegDraftSaleLineItemFactory;

	@Inject
	public ConsumerSaleRegDraftFactoryImpl(
			@NonNull ConsumerSaleRegDraftSaleLineItemFactory partnerSaleRegDraftSaleLineItemFactory
			){
		this.consumerSaleRegDraftSaleLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleLineItemFactory",
                        partnerSaleRegDraftSaleLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();
	}

	@Override
    public ConsumerPartnerSaleRegDraft construct(
            @NonNull AddConsumerSaleRegDraftReq addConsumerSaleRegDraftReq
    ) {

        ConsumerPartnerSaleRegDraft objectUnderConstruction =
                new ConsumerPartnerSaleRegDraft();

		        objectUnderConstruction.setFirstName(
					        		addConsumerSaleRegDraftReq
					        		.getFirstName()
					        		.getValue()
		        				);
        
		        objectUnderConstruction.setLastName(
		        		addConsumerSaleRegDraftReq
		        		.getLastName()
		        		.getValue()
		        	);
        
	    		objectUnderConstruction.setAddressLine1(
										addConsumerSaleRegDraftReq
				                        .getAddress()
				                        .getAddressLine1()
				                        .getValue()
	                        				);
	    		
	    		objectUnderConstruction.setAddressLine2(
						addConsumerSaleRegDraftReq
                        .getAddress()
                        .getAddressLine2()
                        .getValue()
            				);
	    		
        		objectUnderConstruction.setCity(
        				addConsumerSaleRegDraftReq
                        .getAddress()
                        .getCity()
                        .getValue()
        				);
        		
        		objectUnderConstruction.setRegion(
        				addConsumerSaleRegDraftReq
                        .getAddress()
                        .getRegionIso31662Code()
                        .getValue()
        				);
        		
        		objectUnderConstruction.setPostalCode(
		        					addConsumerSaleRegDraftReq
			                        .getAddress()
			                        .getPostalCode()
			                        .getValue()
	                        );

        		objectUnderConstruction.setCountry(
        				addConsumerSaleRegDraftReq
	                        .getAddress()
	                        .getCountryIso31661Alpha2Code()
	                        .getValue()
	                       );
		 
        		objectUnderConstruction.setPhone(
        					addConsumerSaleRegDraftReq
	                        .getPhoneNumber()
	                        .isPresent()
	                        ?
                    		 addConsumerSaleRegDraftReq
                                     .getPhoneNumber()
                                     .get()
                                     .getValue()
                             : null
				 			);
        		
	        	objectUnderConstruction.setEmail(
					 addConsumerSaleRegDraftReq
					 .getPersonEmail()
					 .isPresent()
					 ?
            		 addConsumerSaleRegDraftReq
                             .getPersonEmail()
                             .get()
                             .getValue()
                     : null
					 );
      
		        objectUnderConstruction.setPartnerAccountId(
		                addConsumerSaleRegDraftReq
		                        .getPartnerAccountId()
		                        .getValue()
		        		);
        
      objectUnderConstruction.setInvoiceNumber(
        		addConsumerSaleRegDraftReq
        			.getInvoiceNumber()
        			.getValue()
        		);

        objectUnderConstruction.setInvoiceUrl(
        		addConsumerSaleRegDraftReq
        			.getInvoiceUrl()
        			.isPresent()
                ? addConsumerSaleRegDraftReq
                        .getInvoiceUrl()
                        .get()
                        .getValue()
                : null
    		);

        objectUnderConstruction.setIsSubmitted(
        		addConsumerSaleRegDraftReq
        			.getIsSubmitted()
        			.getValue()
        		);


        if(((Collection<ConsumerSaleLineItem>)addConsumerSaleRegDraftReq.getSaleLineItems()).size()>0){
	        objectUnderConstruction.setSaleLineItems(
	        		StreamSupport
	                	.stream(
	                		addConsumerSaleRegDraftReq
	            				.getSaleLineItems()
	                                .spliterator(),
	                        false)
	        			.map(consumerSaleRegDraftSaleLineItemFactory :: construct)
	        			.collect(Collectors.toList())
	        		);
        }

        objectUnderConstruction.setSalePartnerRepId(
        		addConsumerSaleRegDraftReq
        			.getUserId()
        			.isPresent()
                ? addConsumerSaleRegDraftReq
                        .getUserId()
                        .get()
                        .getValue()
                : null
    		);


        	objectUnderConstruction.setSellDate(
        			addConsumerSaleRegDraftReq
        				.getSellDate()
        				.getValue()
        				);
        	
        	 objectUnderConstruction.setSaleCreatedAtTimestamp(
             		new InstantPersistenceConverter()
             		.convertToEntityAttribute(new Timestamp(new Date().getTime()))
             		);
        	 
        	 objectUnderConstruction.setCreatedDate(
  	        		new InstantPersistenceConverter()
  	        		.convertToEntityAttribute(new Timestamp(new Date().getTime()))
  	        		);

        return objectUnderConstruction;

    }
}