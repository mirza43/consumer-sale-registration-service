package com.precorconnect.consumersaleregistrationservice.database;

import static com.precorconnect.guardclauses.Guards.guardThat;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.checkerframework.checker.nullness.qual.NonNull;

import com.precorconnect.consumersaleregistrationservice.ConsumerSaleLineItem;

@Singleton
public class UpdateConsumerSaleRegDraftSaleLineItemFactoryImpl implements UpdateConsumerSaleRegDraftSaleLineItemFactory {
	
	 /*
    fields
     */
    private final UpdateConsumerSaleRegDraftSaleSimpleLineItemFactory partnerSaleRegDraftSaleSimpleLineItemFactory;

    private final UpdateConsumerSaleRegDraftSaleCompositeLineItemFactory partnerSaleRegDraftSaleCompositeLineItemFactory;
    
    private final UpdateConsumerSaleRegDraftSaleSimpleLineItemReqFactory updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory;
    
    private final UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactory updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory;
    
    
    

    @Inject
	public UpdateConsumerSaleRegDraftSaleLineItemFactoryImpl(
			UpdateConsumerSaleRegDraftSaleSimpleLineItemFactory partnerSaleRegDraftSaleSimpleLineItemFactory,
			UpdateConsumerSaleRegDraftSaleCompositeLineItemFactory partnerSaleRegDraftSaleCompositeLineItemFactory,
			UpdateConsumerSaleRegDraftSaleSimpleLineItemReqFactory updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory,
			UpdateConsumerSaleRegDraftSaleCompositeLineItemReqFactory updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory) {
		
		this.partnerSaleRegDraftSaleSimpleLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleSimpleLineItemFactory",
                        partnerSaleRegDraftSaleSimpleLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.partnerSaleRegDraftSaleCompositeLineItemFactory =
                guardThat(
                        "partnerSaleRegDraftSaleCompositeLineItemFactory",
                        partnerSaleRegDraftSaleCompositeLineItemFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		
		this.updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory =
                guardThat(
                        "updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory",
                        updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory
                )
                        .isNotNull()
                        .thenGetValue();
		
		this.updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory =
                guardThat(
                        "updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory",
                        updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory
                )
                        .isNotNull()
                        .thenGetValue();
				
		
	}

	@Override
	public com.precorconnect.consumersaleregistrationservice.database.ConsumerSaleRegDraftSaleLineItem construct(@NonNull ConsumerSaleLineItem partnerSaleLineItem) {
		
		
		if (partnerSaleLineItem instanceof
                com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem) {
            return
            		updatePartnerSaleRegDraftSaleSimpleLineItemReqFactory
                            .construct(
                                    (com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleSimpleLineItem)
                                            partnerSaleLineItem
                            );
        } else if (partnerSaleLineItem instanceof
                com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem) {
            return
            		updatePartnerSaleRegDraftSaleCompositeLineItemReqFactory
                            .construct(
                                    (com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleCompositeLineItem)
                                            partnerSaleLineItem
                            );
        }
		
		 // handle unexpected implementations
        throw
                new RuntimeException(
                        String.format(
                                "received unsupported %s implementation: %s",
                                com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem.class,
                                partnerSaleLineItem.getClass()
                        )
                );
            
	}

	@Override
	public com.precorconnect.consumersaleregistrationservice.ConsumerSaleRegDraftSaleLineItem construct(
			com.precorconnect.consumersaleregistrationservice.database.@NonNull ConsumerSaleRegDraftSaleLineItem partnerSaleRegDraftSaleLineItem) {
		
		
		if (partnerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleLineItem) {

            return
                    partnerSaleRegDraftSaleSimpleLineItemFactory
                            .construct(
                                    (ConsumerSaleRegDraftSaleSimpleLineItem) partnerSaleRegDraftSaleLineItem
                            );

        } else if (partnerSaleRegDraftSaleLineItem instanceof ConsumerSaleRegDraftSaleCompositeLineItem) {

            return
                    partnerSaleRegDraftSaleCompositeLineItemFactory
                            .construct(
                                    (ConsumerSaleRegDraftSaleCompositeLineItem) partnerSaleRegDraftSaleLineItem
                            );

        }
        // handle unexpected implementations
        throw
                new RuntimeException(
                        String.format(
                                "received unsupported %s implementation: %s",
                                ConsumerSaleRegDraftSaleLineItem.class,
                                partnerSaleRegDraftSaleLineItem.getClass()
                        )
                );
        
	}

}
