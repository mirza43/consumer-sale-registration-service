package com.precorconnect.consumersaleregistrationservice.database;

import org.checkerframework.checker.nullness.qual.NonNull;

public interface UpdateConsumerSaleRegDraftSaleSimpleLineItemReqFactory {
	
	ConsumerSaleRegDraftSaleSimpleLineItem construct(
            com.precorconnect.consumersaleregistrationservice.
            @NonNull ConsumerSaleRegDraftSaleSimpleLineItem partnerSaleRegDraftSaleSimpleLineItem
    );
    

}
