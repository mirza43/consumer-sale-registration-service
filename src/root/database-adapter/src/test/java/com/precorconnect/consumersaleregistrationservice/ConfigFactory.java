package com.precorconnect.consumersaleregistrationservice;

import com.precorconnect.consumersaleregistrationservice.database.DatabaseAdapterConfigFactoryImpl;

public final class ConfigFactory {
	
	public Config construct(){
		
		return
				new Config(
					new DatabaseAdapterConfigFactoryImpl()
														 .construct()
				);
	}

}
